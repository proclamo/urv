
Instal·lació
============

1. Descarregar el repositori:

git clone git@bitbucket.org:proclamo/urv.git


2. crear directori uploads:

mkdir -p web/uploads/images
mkdir -p web/uploads/pdfs

3. permissos

HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
sudo chmod +a "$HTTPDUSER allow delete,delete_child,write,append,file_inherit,directory_inherit" app/cache app/logs web/uploads/images web/uploads/pdfs
sudo chmod +a "`whoami` allow delete,delete_child,write,append,file_inherit,directory_inherit" app/cache app/logs web/uploads/images web/uploads/pdfs

4. Crear i/o modificar l'arxiu app/config/parameters.yml
   Ha de ser semblant a això:

parameters:
    database_driver: pdo_pgsql
    database_host: host
    database_port: 5432
    database_name: urv
    database_user: username
    database_password: password
    mailer_transport: smtp
    mailer_host: 127.0.0.1
    mailer_user: null
    mailer_password: null
    locale: ca
    secret: departamentgestioiempresaurv
    debug_toolbar: true
    debug_redirects: false
    use_assetic_controller: true

5. instalar composer:

curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

6. baixar dependències
composer install

7. comprobar requeriments:

http://host/config.php


8. tasques bbdd:

php app/console doctrine:database:create
php app/console doctrine:schema:update --force
php app/console fos:user:create
    - nom per a un administrador
    - email del administrador
    - password
php app/console fos:user:promote
    - ROLE_SUPER_ADMIN

