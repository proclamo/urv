<?php

namespace Proclamo\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Proclamo\AppBundle\Repository\PublicacioRepository")
 * @ORM\Table(name = "publicacio")
 */
class Publicacio {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type = "string", nullable = false)
     */
    protected $grup;
    
    /**
     *
     * @ORM\Column(type = "string", nullable = true)
     */
    protected $originalgrup;

    /**
     * @ORM\Column(type = "integer", name = "ani")
     */
    protected $any;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type = "string")
     */
    protected $titol;

    /**
     * @ORM\Column(type = "text", nullable = true)
     */
    protected $referencia;

    /**
     * @ORM\Column(type = "string", nullable = true)
     */
    protected $ciutat;

    /**
     * @ORM\Column(type = "string", nullable = true)
     */
    protected $pais;

    /**
     * @ORM\Column(type = "string", nullable = true)
     */
    protected $volum;

    /**
     * @ORM\Column(type = "string", nullable = true)
     */
    protected $pagines;

    /**
     * @ORM\Column(type = "string", nullable = true)
     */
    protected $nomMedi;

    /**
     * @ORM\OneToMany(targetEntity = "PublicacioAutors", mappedBy = "publicacio", cascade={"all"})
     */
    protected $autors;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     */
    private $locale;

    public function __construct($locale = null) {
        if (!empty($locale)) {
            $this->locale = $locale;
        }

        $this->autors = new ArrayCollection();
    }

    function getId() {
        return $this->id;
    }

    function getGrup() {
        return $this->grup;
    }

    function getAny() {
        return $this->any;
    }

    function getTitol() {
        return $this->titol;
    }

    function getVolum() {
        return $this->volum;
    }

    function getPagines() {
        return $this->pagines;
    }

    function getNomMedi() {
        return $this->nomMedi;
    }

    function getAutors() {
        return $this->autors;
    }

    function getLocale() {
        return $this->locale;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setGrup($grup) {
        $this->grup = $grup;
    }

    function setAny($any) {
        $this->any = $any;
    }

    function setTitol($titol) {
        $this->titol = $titol;
    }

    function setVolum($volum) {
        $this->volum = $volum;
    }

    function setPagines($pagines) {
        $this->pagines = $pagines;
    }

    function setNomMedi($nomMedi) {
        $this->nomMedi = $nomMedi;
    }

    function setAutors($autors) {
        $this->autors = $autors;
    }

    public function addAutor($autor) {
        $this->autors->add($autor);
    }

    public function removeAutor($autor) {
        $this->autors->removeElement($autor);
    }

    function setLocale($locale) {
        $this->locale = $locale;
    }

    function getCiutat() {
        return $this->ciutat;
    }

    function getPais() {
        return $this->pais;
    }

    function setCiutat($ciutat) {
        $this->ciutat = $ciutat;
    }

    function setPais($pais) {
        $this->pais = $pais;
    }

    function getReferencia() {
        return $this->referencia;
    }

    function setReferencia($referencia) {
        $this->referencia = $referencia;
    }
    
    function getOriginalgrup() {
      return $this->originalgrup;
    }

    function setOriginalgrup($originalgrup) {
      $this->originalgrup = $originalgrup;
    }
}
