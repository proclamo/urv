<?php

namespace Proclamo\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Proclamo\AppBundle\Repository\NoticiaRepository")
 * @ORM\Table(name = "noticia")
 */
class Noticia {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type = "string")
     */
    protected $titol;

    /**
     * @ORM\Column(type = "text")
     */
    protected $texte;

    /**
     * @ORM\Column(type = "datetime")
     */
    protected $data;

    /**
     * @Gedmo\Slug(fields={"titol"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    function getId() {
        return $this->id;
    }

    function getTitol() {
        return $this->titol;
    }

    function getTexte() {
        return $this->texte;
    }

    function getData() {
        return $this->data;
    }

    function getSlug() {
        return $this->slug;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTitol($titol) {
        $this->titol = $titol;
    }

    function setTexte($texte) {
        $this->texte = $texte;
    }

    function setData($data) {
        $this->data = $data;
    }

    function setSlug($slug) {
        $this->slug = $slug;
    }


}
