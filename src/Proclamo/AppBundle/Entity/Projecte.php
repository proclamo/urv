<?php

namespace Proclamo\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Proclamo\AppBundle\Repository\ProjecteRepository")
 * @ORM\Table(name = "projecte")
 */
class Projecte {
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @Gedmo\Translatable
     * @ORM\Column(type = "string", nullable = true)
     */
    protected $grup;
    
    /**
     * @ORM\Column(type = "string")
     */
    protected $titol;
    
    /**
     * @ORM\ManyToOne(targetEntity = "Personal")
     */
    protected $investigador;
    
    
    /**
     * @ORM\ManyToMany(targetEntity="Personal", cascade={"persist"}) 
     * @ORM\JoinTable(name="projecte_personal",
     *      joinColumns={@ORM\JoinColumn(name="projecte_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="personal_id", referencedColumnName="id")}
     *      )
     */
    protected $autors;
    
    /**
     * @ORM\ManyToMany(targetEntity="Colaborador", cascade={"persist"}) 
     * @ORM\JoinTable(name="projecte_colaborador",
     *      joinColumns={@ORM\JoinColumn(name="projecte_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="colaborador_id", referencedColumnName="id")}
     *      )
     */
    protected $colaboradors;
    
    /**
     * @ORM\Column(type = "string", nullable = true)
     */
    protected $codi;
    
    /**
     * @ORM\Column(type = "integer")
     */
    protected $inici;
    
    /**
     * @ORM\Column(type = "integer", nullable = true)
     */
    protected $final;
    
    /**
     * @ORM\Column(type = "text", nullable = true)
     */
    protected $descripcio;
    
    /**
     * @ORM\Column(type = "string", nullable = true)
     */
    protected $link;    
    
    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     */
    private $locale;
    
    public function __construct($locale = null) {
        
        $this->autors = new ArrayCollection();
        $this->colaboradors = new ArrayCollection();
        
        if (!empty($locale)) {
            $this->locale = $locale;
        }
    }
    
    function getId() {
        return $this->id;
    }

    function getGrup() {
        return $this->grup;
    }

    function getTitol() {
        return $this->titol;
    }

    function getInvestigador() {
        return $this->investigador;
    }

    function getAutors() {
        return $this->autors;
    }

    function getColaboradors() {
        return $this->colaboradors;
    }

    function getCodi() {
        return $this->codi;
    }

    function getInici() {
        return $this->inici;
    }

    function getFinal() {
        return $this->final;
    }

    function getDescripcio() {
        return $this->descripcio;
    }

    function getLink() {
        return $this->link;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setGrup($grup) {
        $this->grup = $grup;
    }

    function setTitol($titol) {
        $this->titol = $titol;
    }

    function setInvestigador($investigador) {
        $this->investigador = $investigador;
    }

    function setAutors($autors) {
        $this->autors = $autors;
    }

    function setColaboradors($colaboradors) {
        $this->colaboradors = $colaboradors;
    }

    function setCodi($codi) {
        $this->codi = $codi;
    }

    function setInici($inici) {
        $this->inici = $inici;
    }

    function setFinal($final) {
        $this->final = $final;
    }

    function setDescripcio($descripcio) {
        $this->descripcio = $descripcio;
    }

    function setLink($link) {
        $this->link = $link;
    }

    public function addAutor($autor)
    {
        $this->autors->add($autor);
    }
    
    public function removeAutor($autor)
    {
        $this->autors->removeElement($autor);
    }
    
    public function addColaborador($colaborador)
    {
        $this->colaboradors->add($colaborador);
    }
    
    public function removeColaborador($colaborador)
    {
        $this->colaboradors->removeElement($colaborador);
    }
}
