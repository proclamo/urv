<?php

namespace Proclamo\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Proclamo\AppBundle\Repository\EventRepository")
 * @ORM\Table(name = "event")
 */
class Event {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type = "string")
     */
    protected $titol;

    /**
     * @ORM\Column(type = "text")
     */
    protected $texte;

    /**
     * @ORM\Column(type = "date")
     */
    protected $data;

    /**
     * @ORM\Column(type = "time", nullable = true)
     */
    protected $hora;

    /**
     * @ORM\OneToOne(targetEntity = "Image", cascade={"persist","remove"})
     */
    protected $foto;

    /**
     * @Gedmo\Slug(fields={"titol"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    function getId() {
        return $this->id;
    }

    function getTitol() {
        return $this->titol;
    }

    function getTexte() {
        return $this->texte;
    }

    function getData() {
        return $this->data;
    }

    function getFoto() {
        return $this->foto;
    }

    function getSlug() {
        return $this->slug;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTitol($titol) {
        $this->titol = $titol;
    }

    function setTexte($texte) {
        $this->texte = $texte;
    }

    function setData($data) {
        $this->data = $data;
    }

    function setFoto($foto) {
        $this->foto = $foto;
    }

    function setSlug($slug) {
        $this->slug = $slug;
    }

    function getHora() {
        return $this->hora;
    }

    function setHora($hora) {
        $this->hora = $hora;
    }
}
