<?php

namespace Proclamo\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

/**
 * @ORM\Entity(repositoryClass="Proclamo\AppBundle\Repository\DocenciaRepository")
 * @ORM\Table(name = "docencia")
 */
class Docencia {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type = "string")
     */
    protected $nom;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type = "string", nullable = true)
     */
    protected $ruta;

    /**
     * @ORM\Column(type = "integer", nullable = true)
     */
    protected $posicio;

    /**
     * @ORM\ManyToOne(targetEntity = "Personal")
     */
    protected $profesor;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     */
    private $locale;

    public function __construct($locale = null) {
        if (!empty($locale)) {
            $this->locale = $locale;
        }
    }

    function getId() {
        return $this->id;
    }

    function getNom() {
        return $this->nom;
    }

    function getRuta() {
        return $this->ruta;
    }

    function getPosicio() {
        return $this->posicio;
    }

    function getProfesor() {
        return $this->profesor;
    }

    function getLocale() {
        return $this->locale;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }

    function setRuta($ruta) {
        $this->ruta = $ruta;
    }

    function setPosicio($posicio) {
        $this->posicio = $posicio;
    }

    function setProfesor($profesor) {
        $this->profesor = $profesor;
    }

    function setLocale($locale) {
        $this->locale = $locale;
    }

}
