<?php

namespace Proclamo\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Proclamo\AppBundle\Repository\PaginaRepository")
 * @ORM\Table(name = "pagina")
 */
class Pagina {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type = "string", nullable = false, options = { "default" = "departament" })
     */
    protected $grup;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type = "string")
     */
    protected $titol;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type = "text")
     */
    protected $texte;

    /**
     * @ORM\OneToOne(targetEntity = "Image", cascade={"persist","remove"})
     */
    protected $foto;

    /**
     * @ORM\Column(type = "integer")
     */
    protected $ordre;

    /**
     * @Gedmo\Translatable
     * @Gedmo\Slug(fields={"titol"})
     * @ORM\Column(type="string", length=128, unique=true)
     */
    protected $slug;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     */
    private $locale;

    public function __construct($locale = null) {
        if (!empty($locale)) {
            $this->locale = $locale;
        }
    }

    function getId() {
        return $this->id;
    }

    function getTitol() {
        return $this->titol;
    }

    function getTexte() {
        return $this->texte;
    }

    function getFoto() {
        return $this->foto;
    }

    function getSlug() {
        return $this->slug;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTitol($titol) {
        $this->titol = $titol;
    }

    function setTexte($texte) {
        $this->texte = $texte;
    }

    function setFoto($foto) {
        $this->foto = $foto;
    }

    function setSlug($slug) {
        $this->slug = $slug;
    }

    function getLocale() {
        return $this->locale;
    }

    function setLocale($locale) {
        $this->locale = $locale;
    }

    function getOrdre() {
        return $this->ordre;
    }

    function setOrdre($ordre) {
        $this->ordre = $ordre;
    }

    function getGrup() {
        return $this->grup;
    }

    function setGrup($grup) {
        $this->grup = $grup;
    }

}
