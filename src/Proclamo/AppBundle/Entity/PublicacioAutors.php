<?php

namespace Proclamo\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Proclamo\AppBundle\Repository\PublicacioAutorsRepository")
 * @ORM\Table(name = "publicacio_autors")
 */
class PublicacioAutors {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity = "Publicacio", inversedBy = "autors")
     */
    protected $publicacio;

    /**
     * @ORM\ManyToOne(targetEntity = "Personal")
     */
    protected $personal;

    /**
     * @ORM\ManyToOne(targetEntity = "Colaborador")
     */
    protected $colaborador;

    /**
     * @ORM\Column(type = "integer", nullable = false)
     */
    protected $ordre;
    
    public function getNomCognoms()
    {
        if ($this->personal != null) {
            return $this->personal->getNomCognoms();
        }
        
        if ($this->colaborador != null) {
            return $this->colaborador->getNom();
        }
    }

    function getId() {
        return $this->id;
    }

    function getPublicacio() {
        return $this->publicacio;
    }

    function getPersonal() {
        return $this->personal;
    }

    function getColaborador() {
        return $this->colaborador;
    }

    function getOrdre() {
        return $this->ordre;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setPublicacio($publicacio) {
        $this->publicacio = $publicacio;
    }

    function setPersonal($personal) {
        $this->personal = $personal;
    }

    function setColaborador($colaborador) {
        $this->colaborador = $colaborador;
    }

    function setOrdre($ordre) {
        $this->ordre = $ordre;
    }    
}
