<?php

namespace Proclamo\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Proclamo\AppBundle\Repository\ModelRepository")
 * @ORM\Table(name = "model")
 */
class Model {

  /**
   * @ORM\Id
   * @ORM\Column(type="integer")
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  protected $id;

  /**
   * @ORM\Column(type = "string")
   */
  protected $grup;

  /**
   * @ORM\Column(type = "string")
   */
  protected $nom;

  /**
   * @ORM\Column(type = "integer")
   */
  protected $posicio;

  /**
   * @ORM\OneToOne(targetEntity = "PDF", cascade={"persist","remove"})
   */
  protected $arxiu;

  /**
   * @ORM\Column(type = "string", nullable = true)
   */
  protected $locale;

  public function __construct($locale = "ca") {
    $this->locale = $locale;
  }

  function getId() {
    return $this->id;
  }

  function getGrup() {
    return $this->grup;
  }

  function getNom() {
    return $this->nom;
  }

  function getPosicio() {
    return $this->posicio;
  }

  function getArxiu() {
    return $this->arxiu;
  }

  function setId($id) {
    $this->id = $id;
  }

  function setGrup($grup) {
    $this->grup = $grup;
  }

  function setNom($nom) {
    $this->nom = $nom;
  }

  function setPosicio($posicio) {
    $this->posicio = $posicio;
  }

  function setArxiu($arxiu) {
    $this->arxiu = $arxiu;
  }

  function getLocale() {
    return $this->locale;
  }

  function setLocale($locale) {
    $this->locale = $locale;
  }
}
