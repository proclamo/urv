<?php

namespace Proclamo\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Proclamo\AppBundle\Repository\PersonalRepository")
 * @ORM\Table(name = "personal")
 */
class Personal {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type = "string", length = 250)
     */
    protected $nom;

    /**
     * @ORM\Column(type = "string", length = 250)
     */
    protected $cognoms;

    /**
     * @ORM\Column(type = "string", length = 250)
     */
    protected $jornada;

    /**
     * @ORM\Column(type = "string", length = 250)
     */
    protected $tipus;

    /**
     * @ORM\Column(type = "string", length = 255, nullable = true)
     */
    protected $email;

    /**
     * @ORM\Column(type = "string", length = 255, nullable = true)
     */
    protected $telefon;

    /**
     * @ORM\Column(type = "string", length = 250, nullable = true)
     */
    protected $despatx;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type = "text", nullable = true)
     */
    protected $perfil;

    /**
     * @ORM\Column(type = "string", length = 255, nullable = true)
     */
    protected $especialitat;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type = "string", length = 255, nullable = true)
     */
    protected $grup;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type = "string", length = 255, nullable = true)
     */
    protected $grupurv;

    /**
     * @ORM\Column(type = "string", length = 255, nullable = true)
     */
    protected $situacio;

    /**
     * @ORM\OneToOne(targetEntity = "PDF", cascade={"persist","remove"})
     */
    protected $curriculum;

    /**
     * @ORM\OneToOne(targetEntity = "PDF", cascade={"persist","remove"})
     */
    protected $cvbreu;

    /**
     * @ORM\OneToOne(targetEntity = "Image", cascade={"persist","remove"})
     */
    protected $foto;

    /**
     * @ORM\Column(type = "string", length = 250, nullable = true)
     */
    protected $nomCognoms;

    /**
     * @ORM\Column(type = "date", nullable = true)
     */
    protected $dataAlta;

    /**
     * @ORM\Column(type = "date", nullable = true)
     */
    protected $dataBaixa;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     */
    private $locale;

    public function __construct($locale = null) {
        if (!empty($locale)) {
            $this->locale = $locale;
        }

        $this->setNomCognoms();
    }

    function getNomCognoms() {
        return $this->nomCognoms;
    }

    function setNomCognoms() {
        if ($this->nom !== null && $this->cognoms !== null) {
            $this->nomCognoms = $this->cognoms . ", " . $this->nom;
        }
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getNom() {
        return $this->nom;
    }

    public function setNom($nom) {
        $this->nom = $nom;

        $this->setNomCognoms();
    }

    public function getCognoms() {
        return $this->cognoms;
    }

    public function setCognoms($cognoms) {
        $this->cognoms = $cognoms;

        $this->setNomCognoms();
    }

    public function getJornada() {
        return $this->jornada;
    }

    public function setJornada($jornada) {
        $this->jornada = $jornada;
    }

    public function getTipus() {
        return $this->tipus;
    }

    public function setTipus($tipus) {
        $this->tipus = $tipus;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getTelefon() {
        return $this->telefon;
    }

    public function setTelefon($telefon) {
        $this->telefon = $telefon;
    }

    public function getDespatx() {
        return $this->despatx;
    }

    public function setDespatx($despatx) {
        $this->despatx = $despatx;
    }

    public function getPerfil() {
        return $this->perfil;
    }

    public function setPerfil($perfil) {
        $this->perfil = $perfil;
    }

    public function getEspecialitat() {
        return $this->especialitat;
    }

    public function setEspecialitat($especialitat) {
        $this->especialitat = $especialitat;
    }

    public function getGrup() {
        return $this->grup;
    }

    public function setGrup($grup) {
        $this->grup = $grup;
    }

    function getSituacio() {
        return $this->situacio;
    }

    function setSituacio($situacio) {
        $this->situacio = $situacio;
    }

    public function getCurriculum() {
        return $this->curriculum;
    }

    public function setCurriculum($curriculum) {
        $this->curriculum = $curriculum;
    }

    public function getCvbreu() {
        return $this->cvbreu;
    }

    public function setCvbreu($cvbreu) {
        $this->cvbreu = $cvbreu;
    }

    public function setLocale($locale) {
        $this->locale = $locale;
    }

    function getFoto() {
        return $this->foto;
    }

    function setFoto($foto) {
        $this->foto = $foto;
    }

    function getGrupurv() {
        return $this->grupurv;
    }

    function setGrupurv($grupurv) {
        $this->grupurv = $grupurv;
    }

    public function __toString() {
        return $this->getNomCognoms();
    }

    function getDataAlta() {
        return $this->dataAlta;
    }

    function getDataBaixa() {
        return $this->dataBaixa;
    }

    function setDataAlta($dataAlta) {
        $this->dataAlta = $dataAlta;
    }

    function setDataBaixa($dataBaixa) {
        $this->dataBaixa = $dataBaixa;
    }

}
