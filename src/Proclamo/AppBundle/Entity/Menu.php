<?php

namespace Proclamo\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Proclamo\AppBundle\Repository\MenuRepository")
 * @ORM\Table(name = "menu")
 */
class Menu {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type = "string")
     */
    protected $grup;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type = "string")
     */
    protected $nom;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type = "string")
     */
    protected $ruta;

    /**
     * @ORM\Column(type = "integer")
     */
    protected $posicio;

    /**
     * @ORM\Column(type="integer", nullable = true)
     */
    protected $pagina;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     */
    private $locale;

    public function __construct($locale = null) {
        if (!empty($locale)) {
            $this->locale = $locale;
        }
    }

    function getId() {
        return $this->id;
    }

    function getGrup() {
        return $this->grup;
    }

    function getNom() {
        return $this->nom;
    }

    function getRuta() {
        return $this->ruta;
    }

    function getPosicio() {
        return $this->posicio;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setGrup($grup) {
        $this->grup = $grup;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }

    function setRuta($ruta) {
        $this->ruta = $ruta;
    }

    function setPosicio($posicio) {
        $this->posicio = $posicio;
    }

    function getLocale() {
        return $this->locale;
    }

    function setLocale($locale) {
        $this->locale = $locale;
    }

    function getPagina() {
        return $this->pagina;
    }

    function setPagina($pagina) {
        $this->pagina = $pagina;
    }

}
