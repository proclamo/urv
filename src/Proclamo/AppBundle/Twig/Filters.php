<?php

namespace Proclamo\AppBundle\Twig;

/**
 * Description of Membres
 *
 * @author cristianmartin
 */
class Filters extends \Twig_Extension {

    private $router;
    private $personalHelper;
    private $doctrine;

    public function setRouter($router) {
        $this->router = $router;
    }

    public function setPersonalHelper($personalHelper) {
        $this->personalHelper = $personalHelper;
    }

    public function setDoctrine($doctrine) {
        $this->doctrine = $doctrine;
    }

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter("categoria", array($this, 'categoriaFilter')),
            new \Twig_SimpleFilter("situacio", array($this, 'situacioFilter')),
            new \Twig_SimpleFilter("dedicacio", array($this, 'dedicacioFilter')),
            new \Twig_SimpleFilter("especialitat", array($this, 'especialitatFilter')),
            new \Twig_SimpleFilter("truncate", array($this, 'truncateFilter'))
        );
    }

    public function getFunctions() {
        return array(
            "membres" => new \Twig_Function_Method($this, 'membresFormater'),
            "class" => new \Twig_Function_Method($this, 'getClass')
        );
    }

    public function categoriaFilter($categoria) {
        $categorias = $this->personalHelper->getCategoriaProfesional();
        return $categorias[$categoria];
    }

    public function situacioFilter($situacio) {
        $situacions = $this->personalHelper->getSituacioAdministrativa();
        return $situacions[$situacio];
    }

    public function dedicacioFilter($dedicacio) {
        $dedicacions = $this->personalHelper->getDedicacio();
        return $dedicacions[$dedicacio];
    }

    public function especialitatFilter($especialitat) {
        $especialitats = $this->personalHelper->getEspecialitat();
        return $especialitats[$especialitat];
    }

    public function membresFormater($entity, $inicial = false) {
        $membres = array();

        $className = $this->getClass($entity);

        if ($className == "Proclamo\AppBundle\Entity\Projecte") {
            $membres = $this->autors($entity->getAutors(), $inicial);

            foreach ($entity->getColaboradors() as $c) {
                $nom = $c->getNom();
                array_push($membres, $nom);
            }
        } else if ($className == "Proclamo\AppBundle\Entity\Publicacio") {
            $em = $this->doctrine->getManager();
            $autors = $em->getRepository('AppBundle:PublicacioAutors')->findMembres($entity->getId());
            $membres = $this->autors($autors, $inicial);
        }

        return "<span>" . \implode("</span>,&nbsp;<span>", $membres) . "</span>";
    }

    public function autors($autors, $inicial) {
        $membres = array();

        foreach ($autors as $autor) {

            if ($this->getClass($autor) == "Proclamo\AppBundle\Entity\Personal") {
                if ($inicial) {
                    $nom = $autor->getCognoms() . ", " . $this->capitalizeName($autor->getNom());
                } else {
                    $nom = $autor->getNomCognoms();
                }

                $ruta = "<a href=\"" . $this->router->generate('personal_show', array('id' => $autor->getId())) . "\" >" . $nom . "</a>";
                array_push($membres, $ruta);
            } else {
                array_push($membres, $autor->getNom());
            }
        }

        return $membres;
    }

    public function capitalizeName($name) {
        return \join(" ", array_map(function($part) {
                    return mb_substr($part, 0, 1, 'UTF-8') . ".";
                }, explode(" ", $name)));
    }

    public function truncateFilter($text, $length) {
        if (strlen($text) > $length) {
            $text = substr($text, 0, $length) . '...';
        }

        return $text;
    }

    public function getName() {
        return 'proclamo_extension';
    }

    public function getClass($object) {
        $className = get_class($object);
        $parts = \explode("Proxies\\__CG__\\", $className);
        return \count($parts) > 1 ? $parts[1] : $parts[0];
    }

}
