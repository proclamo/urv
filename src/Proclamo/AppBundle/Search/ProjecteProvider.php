<?php

namespace Proclamo\AppBundle\Search;

use ChubProduction\SearchBundle\Service\SearchProviderInterface;
use ChubProduction\SearchBundle\Service\SearchResult;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Description of PersonalProvider
 *
 * @author cristianmartin
 */
class ProjecteProvider implements SearchProviderInterface {
    
    private $doctrine;
    private $router;
    
    public function setDoctrine($doctrine) {
        $this->doctrine = $doctrine;
    }
    
    public function setRouter($router) {
        $this->router = $router;
    }
    
    public function getName() {
        return 'projecte';
    }

    public function getTitle() {
        return 'Projectes';
    }

    public function search($str) {
        
        $em = $this->doctrine->getManager();
        
        $projectes = $em->getRepository('AppBundle:Projecte')->search($str);
        
        $resultSet = new ArrayCollection();        
        
        foreach ($projectes as $projecte) {
            $result = new SearchResult();
            $result->setId($projecte->getId());
            $result->setTitle($projecte->getTitol());
            $result->setDescription($projecte->getDescripcio());
            $result->setUrl($this->router->generate('personal_show', array('id' => $projecte->getInvestigador()->getId())) . "#projecte_" . $projecte->getId());
            $resultSet->add($result);
        }
        
        return $resultSet;
    }

}
