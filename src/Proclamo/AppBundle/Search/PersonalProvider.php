<?php

namespace Proclamo\AppBundle\Search;

use ChubProduction\SearchBundle\Service\SearchProviderInterface;
use ChubProduction\SearchBundle\Service\SearchResult;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Description of PersonalProvider
 *
 * @author cristianmartin
 */
class PersonalProvider implements SearchProviderInterface {
    
    private $doctrine;
    private $router;
    
    public function setDoctrine($doctrine) {
        $this->doctrine = $doctrine;
    }
    
    public function setRouter($router) {
        $this->router = $router;
    }
    
    public function getName() {
        return 'personal';
    }

    public function getTitle() {
        return 'Personal';
    }

    public function search($str) {
        
        $em = $this->doctrine->getManager();
        
        $personals = $em->getRepository('AppBundle:Personal')->search($str);
        
        $resultSet = new ArrayCollection();        
        
        foreach ($personals as $personal) {
            $result = new SearchResult();
            $result->setId($personal->getId());
            $result->setTitle($personal->getNomCognoms());
            $result->setDescription($personal->getPerfil());
            $result->setUrl($this->router->generate('personal_show', array('id' => $personal->getId())));
            $resultSet->add($result);
        }
        
        return $resultSet;
    }

}
