<?php

namespace Proclamo\AppBundle\Search;

use ChubProduction\SearchBundle\Service\SearchProviderInterface;
use ChubProduction\SearchBundle\Service\SearchResult;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Description of PersonalProvider
 *
 * @author cristianmartin
 */
class PublicacioProvider implements SearchProviderInterface {
    
    private $doctrine;
    private $router;
    
    public function setDoctrine($doctrine) {
        $this->doctrine = $doctrine;
    }
    
    public function setRouter($router) {
        $this->router = $router;
    }
    
    public function getName() {
        return 'publicacio';
    }

    public function getTitle() {
        return 'Publicacions';
    }

    public function search($str) {
        
        $em = $this->doctrine->getManager();
        
        $publicacions = $em->getRepository('AppBundle:Publicacio')->search($str);
        
        $resultSet = new ArrayCollection();        
        
        foreach ($publicacions as $publicacio) {
            $result = new SearchResult();
            $result->setId($publicacio->getId());
            $result->setTitle($publicacio->getTitol());
            $result->setDescription($publicacio->getGrup());
            
            // selecciono el primer autor que sea personal
            $autor = $this->getAutorPersonal($publicacio->getAutors());
            
            $result->setUrl($this->router->generate('personal_show', array('id' => $autor->getId())) . "#publicacio_" . $publicacio->getId());
            $resultSet->add($result);
        }
        
        return $resultSet;
    }
    
    
    private function getAutorPersonal($autors)
    {
        foreach ($autors as $autor) {
            if ($autor->getPersonal() != null) {
                return $autor->getPersonal();
            }
        }
    }
}
