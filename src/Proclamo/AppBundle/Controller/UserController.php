<?php

namespace Proclamo\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Proclamo\AppBundle\Entity\User;
use Proclamo\AppBundle\Form\UserType;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/usuaris")
 */
class UserController extends Controller
{
    /**
     * @Route("/", name="usuaris_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $entities = $em->getRepository('AppBundle:User')->findAll();
        
        return $this->render('User/index.html.twig', array(
            'entities' => $entities,
            'title' => 'Tots els usuaris'
        ));
    }

    /**
     * @Route("/new", name="usuari_new")
     * @Method("GET")
     */
    public function newAction()
    {
        $entity = new User();
        $form = $this->createForm(new UserType(), $entity);
        
        return $this->render('User/new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/create", name="usuari_create")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {        
        $entity = new User();
        $form = $this->createForm(new UserType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $manipulator = $this->get('fos_user.util.user_manipulator');
            
            $admin = $form->get("admin")->getData();
            $user = $manipulator->create($entity->getUsername(), $entity->getPassword(), $entity->getEmail(), TRUE, $admin);
            
            return $this->redirect($this->generateUrl('usuaris_index'));
        }

        return $this->render('User/new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }


    /**
     * @Route("/edit/{id}", name="usuari_edit")
     * @Method("GET")
     */
    public function editAction(User $entity)
    {
        $form = $this->createForm(new UserType(), $entity);
        
        $form->get("admin")->setData($entity->hasRole('ROLE_SUPER_ADMIN'));
        
        return $this->render('User/edit.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView()
        ));
    }
    
    /**
     * @Route("/update/{id}", name="usuari_update")
     * @Method("POST")
     */
    public function updateAction(Request $request, User $entity)
    {
        $form = $this->createForm(new UserType(), $entity);
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            
            $manipulator = $this->get('fos_user.util.user_manipulator');
            
            $admin = $form->get("admin")->getData();
            
            if ($entity->hasRole('ROLE_SUPER_ADMIN') && !$admin) {
                $manipulator->demote($entity->getUsername());
            }
            
            if (!$entity->hasRole('ROLE_SUPER_ADMIN') && $admin) {
                $manipulator->promote($entity->getUsername());
            }
            
            $password = $form->get('password')->getData();
            
            if ($password != null) {
                $manipulator->changePassword($entity->getUsername(), $password);
            }
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('usuaris_index'));
        }
        
        return $this->render('User/edit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * @Route("/delete/{id}", name="usuari_delete")
     * @Method("GET")
     */
    public function deleteAction(User $entity)
    {
        $em = $this->getDoctrine()->getManager();
        
        $em->remove($entity);
        $em->flush();
        
        return $this->redirect($this->generateUrl('usuaris_index'));
        
    }

}
