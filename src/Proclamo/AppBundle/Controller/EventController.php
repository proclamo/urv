<?php

namespace Proclamo\AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Proclamo\AppBundle\Entity\Event;
use Proclamo\AppBundle\Form\EventType;

/**
 * @Route("/events")
 */
class EventController extends Controller {

    /**
     * @Route("/{_locale}", name="events_index")
     * @Method("GET")
     */
    public function indexAction(Request $request, $_locale = "ca") {
        $em = $this->getDoctrine()->getManager();
        
        $request->setLocale($_locale);

        $futures = $em->getRepository('AppBundle:Event')->findFutures();
        $passats = $em->getRepository('AppBundle:Event')->findPassats();
        
        return $this->render('Event/index.html.twig', array(
                    'future' => $futures,
                    'passat' => $passats,
                    'title' => 'Events'
        ));
    }

    /**
     * @Route("/resum", name="events_resum")
     * @Method("GET")
     */
    public function resumAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Event')->findFutures(2);

        return $this->render('Event/resum.html.twig', array(
                    'entities' => $entities
        ));
    }

    /**
     * @Route("/event/{slug}/{_locale}", name = "events_show")
     * @Method("GET")
     */
    public function showAction($slug, $_locale = "ca") {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Event')->findOneBySlug($slug);

        return $this->render('Event/show.html.twig', array(
                    'entity' => $entity,
                    '_locale' => $_locale
        ));
    }

    /**
     * @Route("/admin/new", name = "events_new")
     * @Method("GET")
     */
    public function newAction() {
        $entity = new Event();
        $form = $this->createForm(new EventType(), $entity);

        return $this->render('Event/new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView()
        ));
    }

    /**
     * @Route("/admin/create", name = "events_create")
     * @Method("POST")
     */
    public function createAction(Request $request) {
        $entity = new Event();
        $form = $this->createForm(new EventType(), $entity);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('events_index'));
        }

        return $this->render('Event/new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView()
        ));
    }

    /**
     * @Route("/admin/edit/{id}", name = "events_edit")
     * @Method("GET")
     */
    public function editAction(Event $entity) {
        $form = $this->createForm(new EventType(), $entity);

        return $this->render('Event/edit.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView()
        ));
    }

    /**
     * @Route("/admin/update/{id}", name = "events_update")
     * @Method("POST")
     */
    public function updateAction(Event $entity, Request $request) {

        $form = $this->createForm(new EventType(), $entity);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('events_show', array('slug' => $entity->getSlug())));
        }

        return $this->render('Event/edit.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView()
        ));
    }

    /**
     * @Route("/admin/{id}", name="events_delete")
     * @Method("GET")
     */
    public function deleteAction(Event $entity) {
        $em = $this->getDoctrine()->getManager();

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('events_index'));
    }
    
    /**
     * @Route("/deleteimgevent/{id}", name="delete_image_event_ajax")
     * @Method("POST")
     */
    public function deleteImageAction(Event $entity) {
        $em = $this->getDoctrine()->getManager();

        $entity->setFoto(null);
        $em->flush();

        $response = new Response(json_encode(array("message" => "Imatge eliminada")));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
