<?php

namespace Proclamo\AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Proclamo\AppBundle\Entity\Projecte;
use Proclamo\AppBundle\Form\ProjecteType;
use Proclamo\AppBundle\Entity\Personal;
use Doctrine\Common\Collections\ArrayCollection;
use Proclamo\AppBundle\Entity\Colaborador;

/**
 * Projecte controller.
 *
 * @Route("/projecte")
 */
class ProjecteController extends Controller
{

    /**
     * Displays a form to create a new Projecte entity.
     *
     * @Route("/new/{id}", name="projecte_new")
     * @Method("GET")
     */
    public function newAction(Personal $personal)
    {
        $entity = new Projecte();
        $entity->setInvestigador($personal);
        
        $form = $this->createForm(new ProjecteType(), $entity);

        return $this->render('Projecte/new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'personal'=> $personal
        ));
    }
    
    /**
     * Creates a new Projecte entity.
     *
     * @Route("/admin/{id}", name="projecte_create")
     * @Method("POST")
     */
    public function createAction(Request $request, Personal $personal)
    {
        $entity = new Projecte();
        $entity->setInvestigador($personal);
        $form = $this->createForm(new ProjecteType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            if ($request->get("membres") != null) {
                $membres = new ArrayCollection($request->get("membres"));
                $entity = $this->addOrCreateMembers($em, $entity, $membres);
            }
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('personal_show', array('id' => $personal->getId())));
        }

        return $this->render('Projecte/new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'personal' => $personal
        ));
    }
    
    
    private function addOrCreateMembers($em, $entity, $membres)
    {
        foreach ($membres as $membre) {
            $autor = $em->getRepository('AppBundle:Personal')->findByNomCognoms($membre);
            if ($autor) {
                $entity->addAutor($autor);

                // lo quitamos del array para que al procesar los colaboradores no lo cree
                $membres->removeElement($membre);
            }
        }

        foreach ($membres as $membre) {
            $autor = $em->getRepository('AppBundle:Colaborador')->findByNomCognoms($membre);

            if ($autor) {
                $entity->addColaborador($autor);
            }
            else {
                $colaborador = new Colaborador();
                $colaborador->setNom($membre);                    
                $em->persist($colaborador);

                $entity->addColaborador($colaborador);
            }
        }

        return $entity;
    }
    
    /**
     * Displays a form to edit an existing Projecte entity.
     *
     * @Route("/admin/{_locale}/{personal}/{id}/edit", name="projecte_edit")
     * @Method("GET")
     */
    public function editAction($_locale, Personal $personal, Projecte $entity)
    {
        $em = $this->getDoctrine()->getManager();

        $editForm = $this->createForm(new ProjecteType(), $entity);

        return $this->render('Projecte/edit.html.twig', array(
            'entity'      => $entity,
            'form'        => $editForm->createView(),
            '_locale'     => $_locale,
            'personal'    => $personal
        ));
    }

    
    /**
     * Edits an existing Projecte entity.
     *
     * @Route("/admin/{_locale}/{personal}/{id}", name="projecte_update")
     * @Method("POST")
     * 
     */
    public function updateAction(Request $request, $personal, Projecte $entity, $_locale)
    {
        $em = $this->getDoctrine()->getManager();

        $editForm = $this->createForm(new ProjecteType(), $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            
            if ($request->get("membres") != null) {
                $membres = new ArrayCollection($request->get("membres"));
                $entity = $this->addOrCreateMembers($em, $entity, $membres);
            }
            $em->flush();

            return $this->redirect($this->generateUrl('personal_show', array('id' => $personal)));
        }

        return $this->render('Projecte/edit.html.twig', array(
            'entity'      => $entity,
            'form'        => $editForm->createView(),
            '_locale'     => $_locale,
            'personal'    => $personal
        ));
    }
    
    
    /**
     * Deletes a Publicacio entity.
     *
     * @Route("/admin/{personal}/{id}", name="projecte_delete")
     * @Method("GET")
     */
    public function deleteAction(Projecte $entity, $personal)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($entity);
        $em->flush();
        
        return $this->redirect($this->generateUrl('personal_show', array('id' => $personal)));
    }
    
    /**
     * @Route("/agrupacio/search", name = "projectes_search_agrupacio")
     */
    public function searchAgrupacio() {
        $q = $this->getRequest()->get('term');
        
        $em = $this->getDoctrine()->getManager();
        
        $ret = $this->searchAgrupacions($q);
        
        $response = new Response(json_encode(array("data" => $ret)));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    
    private function searchAgrupacions($q)
    {
        $em = $this->getDoctrine()->getManager();        
        $repository = $em->getRepository("AppBundle:Projecte");
        
        if ($q == null) {
            return $repository->findByGrup();
        }
        else {
            return $repository->searchAgrupacions($q);
        }        
    }
}
