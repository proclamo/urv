<?php

namespace Proclamo\AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Proclamo\AppBundle\Entity\Menu;
use Proclamo\AppBundle\Form\MenuType;

/**
 * @Route("/admin/menus")
 */
class MenuController extends Controller 
{
    /**
     * @Route("/{_locale}/", name="menus_index", requirements={ "_locale" = "ca|es|en" })
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $locale = $request->getLocale();
        
        $em = $this->getDoctrine()->getManager();
        
        $entities = array();
        
        $items = $em->getRepository('AppBundle:Menu')->findAllAgrupat($locale);
        
        foreach ($items as $item) {
            $grup = $item["grup"];
            
            if (!isset($entities[$grup])) {
                $entities[$grup] = array("nom" => $grup, "links" => array());
            }
            
            $arr = $entities[$grup]["links"];
            
            array_push($arr, $item);
            
            $entities[$grup]["links"] = $arr;
        }
        
        return $this->render('Menu/index.html.twig', array(
            'entities' => $entities,
            'title' => 'Tots els menus',
            '_locale' => $locale
        ));
    }

    /**
     * @Route("/new", name="menu_new")
     * @Method("GET")
     */
    public function newAction(Request $request)
    {
        $entity = new Menu();
        $form = $this->createForm(new MenuType(), $entity);
        $locale = $request->getLocale();
        
        return $this->render('Menu/new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            '_locale' => $locale
        ));
    }

    /**
     * @Route("/create", name="menu_create")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {        
        $entity = new Menu();
        $form = $this->createForm(new MenuType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            return $this->redirect($this->generateUrl('menus_index'));
        }

        return $this->render('Menu/new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }


    /**
     * @Route("/edit/{_locale}/{id}", name="menu_edit", requirements={ "_locale" = "ca|es|en" })
     * @Method("GET")
     */
    public function editAction(Menu $entity, $_locale)
    {
        $entity->setLocale($_locale);
        $form = $this->createForm(new MenuType(), $entity);
        
        return $this->render('Menu/edit.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            '_locale'     => $_locale
        ));
    }
    
    /**
     * @Route("/update/{_locale}/{id}", name="menu_update", requirements={ "_locale" = "ca|es|en" })
     * @Method("POST")
     */
    public function updateAction(Request $request, Menu $entity, $_locale)
    {
        $form = $this->createForm(new MenuType(), $entity);
        $form->handleRequest($request);
        $entity->setLocale($_locale);
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirect($this->generateUrl('menus_index'));
        }
        
        return $this->render('Menu/edit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            '_locale' => $_locale
        ));
    }

    /**
     * @Route("/delete/{id}", name="menu_delete")
     * @Method("GET")
     */
    public function deleteAction(Menu $entity)
    {
        $em = $this->getDoctrine()->getManager();
        
        $em->remove($entity);
        $em->flush();
        
        //$this->get('session')->getFlashBag()->add();
        
        return $this->redirect($this->generateUrl('menus_index'));
        
    }
//    
//    /**
//     * @Route("/menu/pagina/search", name = "menu_search_paginas")
//     * @Method("POST")
//     */
//    public function buscaPaginas()
//    {
//        $q = $this->getRequest()->get('term');
//        
//        $em = $this->getDoctrine()->getManager();
//        $repository = $em->getRepository("AppBundle:Pagina");
//        $router = $this->get("router");
//        
//        $retorn = $repository->buscaPorTitulo($q);
//        $ret = array();
//        
//        foreach ($retorn as $pagina) {
//            $r = array();
//            $r["titol"] = $pagina["titol"];            
//            $r["ruta"] = $router->generate('departament_show', array('slug' => $pagina["slug"] ));
//            array_push($ret, $r);
//        }
//        
//        $response = new Response(json_encode(array("data" => $ret)));
//        $response->headers->set('Content-Type', 'application/json');
//        return $response;
//    }
}
