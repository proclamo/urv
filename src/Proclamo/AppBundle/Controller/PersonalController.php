<?php

namespace Proclamo\AppBundle\Controller;

use Proclamo\AppBundle\Entity\Personal;
use Proclamo\AppBundle\Lib\PublicacionsHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Personal controller.
 *
 * @Route("/personal")
 */
class PersonalController extends Controller {

    private $pag = 10;
    
    /**
     * Lists all Personal entities.
     *
     * @Route("/{_locale}/{page}", name="personal", requirements={ "_locale" = "ca|es|en", "page" = "\d+" }, defaults={"page" = 1})
     * @Method("GET")
     */
    public function indexAction(Request $request, $page) {
        $em = $this->getDoctrine()->getManager();

        $locale = $request->getLocale();
        
        $result = $em->getRepository('AppBundle:Personal')->findAll($locale);
        
        $paginator = $this->get('knp_paginator');
        
        $entities = $paginator->paginate($result, $page, $this->pag);

        return $this->render('Personal/index.html.twig', array(
                    'entities' => $entities,
                    'title' => 'Tots',
                    'page' => $page
        ));
    }
   
    /**
     * List Personal entities where tipus professor i jornada completa
     * @Route("/{_locale}/professortc/{page}", name="personal_professor_tc", requirements={ "_locale" = "ca|es|en", "page" = "\d+" }, defaults={"page" = 1})
     * @Method("GET")
     */
    public function professorsTC(Request $request, $page) {
        $em = $this->getDoctrine()->getManager();

        $locale = $request->getLocale();

        $tipus = array('catedràtic',
            'catedràtic_universitat',
            "titular_universitat",
            "titular_escola_universitaria",
            "agregat",
            "ajudant",
            "collaborador_permanent",
            "lector",
            "visitant",
            "investigador_SECTI");

        $jornada = 'completa';

        $result = $em->getRepository('AppBundle:Personal')->findByTipusJornada($tipus, $jornada, $locale);
        
        $paginator = $this->get('knp_paginator');
        
        $entities = $paginator->paginate($result, $page, $this->pag);

        return $this->render('Personal/index.html.twig', array(
                    'entities' => $entities,
                    'title' => 'Professorat temps complet',
                    'page' => $page
        ));
    }

    /**
     * List Personal entities where tipus professor i jornada parcial
     * @Route("/{_locale}/professortp/{page}", name="personal_professor_tp", requirements={ "_locale" = "ca|es|en", "page" = "\d+" }, defaults={"page" = 1})
     * @Method("GET")
     */
    public function professorsTP(Request $request, $page) {
        $em = $this->getDoctrine()->getManager();

        $locale = $request->getLocale();

        $tipus = array('associat');
        $jornada = 'parcial';

        $result = $em->getRepository('AppBundle:Personal')->findByTipusJornada($tipus, $jornada, $locale);
        
        $paginator = $this->get('knp_paginator');
        
        $entities = $paginator->paginate($result, $page, $this->pag);

        return $this->render('Personal/index.html.twig', array(
                    'entities' => $entities,
                    'title' => 'Professorat temps parcial',
                    'page' => $page
        ));
    }

    /**
     * List Personal entities where tipus becari i jornada parcial
     * @Route("/{_locale}/becaris/{page}", name="personal_becaris", requirements={ "_locale" = "ca|es|en", "page" = "\d+" }, defaults={"page" = 1})
     * @Method("GET")
     */
    public function becaris(Request $request, $page) {
        $em = $this->getDoctrine()->getManager();

        $locale = $request->getLocale();

        $situacio = array("becari_predoctoral", "becari_AGAUR");
        $jornada = null;

        $result = $em->getRepository('AppBundle:Personal')->findByTipusJornada($situacio, null, $locale);
        
        $paginator = $this->get('knp_paginator');
        
        $entities = $paginator->paginate($result, $page, $this->pag);

        return $this->render('Personal/index.html.twig', array(
                    'entities' => $entities,
                    'title' => 'Becaris',
                    'page' => $page
        ));
    }

    /**
     * List Personal entities where tipus pas
     * @Route("/{_locale}/pas/{page}", name="personal_pas", requirements={ "_locale" = "ca|es|en", "page" = "\d+" }, defaults={"page" = 1})
     * @Method("GET")
     */
    public function pas(Request $request, $page) {
        $em = $this->getDoctrine()->getManager();

        $locale = $request->getLocale();

        $tipus = array("responsable_administratiu",
            "administratiu",
            "tècnic_informàtic",
            "personal_suport_recerca");

        $jornada = 'completa';

        $result = $em->getRepository('AppBundle:Personal')->findByTipusJornada($tipus, $jornada, $locale);
        
        $paginator = $this->get('knp_paginator');
        
        $entities = $paginator->paginate($result, $page, $this->pag);

        return $this->render('Personal/index.html.twig', array(
                    'entities' => $entities,
                    'title' => "Personal d'administració i serveis",
                    'page' => $page
        ));
    }

    /**
     * Displays a form to create a new Personal entity.
     *
     * @Route("/admin/new", name="personal_new")
     * @Method("GET")
     */
    public function newAction() {
        $formType = $this->get('app.personal.form');

        $entity = new Personal();
        $form = $this->createForm($formType, $entity);

        return $this->render('Personal/new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new Personal entity.
     *
     * @Route("/admin/", name="personal_create")
     * @Method("POST")
     */
    public function createAction(Request $request) {
        $formType = $this->get('app.personal.form');

        $entity = new Personal();
        $form = $this->createForm($formType, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getId())));
        }

        return $this->render('Personal/new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Personal entity.
     *
     * @Route("/show/{_locale}/{id}", name="personal_show", requirements={ "_locale" = "ca|es|en" })
     * @Method("GET")
     */
    public function showAction(Request $request, Personal $entity) {
        $em = $this->getDoctrine()->getManager();

        $locale = $request->getLocale();

        $entity->setLocale($locale);
        $em->refresh($entity);

        $docencies = $em->getRepository('AppBundle:Docencia')->getFromPersonal($entity->getNomCognoms(), $locale);

        $_publicacions = $em->getRepository('AppBundle:Publicacio')->getFromPersonal($entity->getId(), $locale);

        $publicacions = $this->agrupa($_publicacions, "publicacions", $locale);

        $_projectes = $em->getRepository('AppBundle:Projecte')->getFromPersonal($entity->getId());

        $projectes = $this->agrupa($_projectes, "projectes", $locale);

        return $this->render('Personal/show.html.twig', array(
                    'entity' => $entity,
                    'docencies' => $docencies,
                    'publicacions' => $publicacions,
                    'projectes' => $projectes
        ));
    }

    private function agrupa($publicacions, $agrupacio, $_locale = "ca") {
        $agrupacions = array();

        foreach ($publicacions as $publicacio) {
            $grup = $publicacio->getGrup();

            if ($grup == "" && $agrupacio != "projectes") {
                $grup = "Altres";
            }

            if (!isset($agrupacions[$grup])) {
                $agrupacions[$grup] = array("nom" => $grup, $agrupacio => array());
            }

            array_push($agrupacions[$grup][$agrupacio], $publicacio);
        }
        
        $pattern = PublicacionsHelper::getPattern($_locale);
        
        uasort($agrupacions, function($a, $b) use ($pattern) {
          $x = array_search(strtolower($a["nom"]), $pattern);
          $y = array_search(strtolower($b["nom"]), $pattern);
          return $x <= $y ? -1 : 1;
        });

        return $agrupacions;
    }

    /**
     * Displays a form to edit an existing Personal entity.
     *
     * @Route("/admin/{_locale}/{id}/edit", name="personal_edit", requirements={ "_locale" = "ca|es|en" })
     * @Method("GET")
     */
    public function editAction($_locale, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Personal')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Personal entity.');
        }

        $entity->setLocale($_locale);
        $em->refresh($entity);

        $formType = $this->get('app.personal.form');

        $editForm = $this->createForm($formType, $entity);

        return $this->render('Personal/edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    '_locale' => $_locale
        ));
    }

    /**
     * Edits an existing Personal entity.
     *
     * @Route("/admin/{_locale}/{id}", name="personal_update", requirements={ "_locale" = "ca|es|en" })
     * @Method("POST")
     */
    public function updateAction(Request $request, $_locale, Personal $entity) {
        $em = $this->getDoctrine()->getManager();

        $entity->setLocale($_locale);
        $em->refresh($entity);

        $formType = $this->get('app.personal.form');

        $editForm = $this->createForm($formType, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getId())));
        }

        return $this->render('Personal/edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    '_locale' => $_locale
        ));
    }

    /**
     * Deletes a Personal entity.
     *
     * @Route("/admin/{id}", name="personal_delete")
     * @Method("GET")
     */
    public function deleteAction(Personal $entity) {
        $em = $this->getDoctrine()->getManager();
        
        $projectes = $em->getRepository('AppBundle:Projecte')->getFromPersonalInvestigadorPrincipal($entity->getId());
        
        foreach ($projectes as $projecte) {
            $em->remove($projecte);
        }
        
        $publicacions = $em->getRepository('AppBundle:Publicacio')->getFromPersonal($entity->getId());
        
        foreach ($publicacions as $publicacio) {
            $em->remove($publicacio);
        }
        
        $docencies = $em->getRepository('AppBundle:Docencia')->getFromPersonal($entity->getNomCognoms());
        
        foreach ($docencies as $docencia) {
            $em->remove($docencia);
        }
        
        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('personal'));
    }

    /**
     * @Route("/search", name="personal_search_nom_json")
     * @return Response
     */
    public function searchJsonAction() {
        $q = $this->getRequest()->get('term');
        $personales = $this->searchPersonal($q);
        $colaboradors = $this->searchColaboradors($q);

        $ret = array();

        foreach ($personales as $personal) {
            array_push($ret, array("nom" => $personal->getNomCognoms()));
        }

        foreach ($colaboradors as $colaborador) {
            array_push($ret, array("nom" => $colaborador->getNom()));
        }

        $response = new Response(json_encode(array("data" => $ret)));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function searchPersonal($q) {
        $em = $this->getDoctrine()->getManager();
        $repop = $em->getRepository("AppBundle:Personal");

        if ($q == null) {
            $personal = $repop->findAll();
        } else {
            $personal = $repop->search($q);
        }

        return $personal;
    }

    public function searchColaboradors($q) {
        $em = $this->getDoctrine()->getManager();
        $repoc = $em->getRepository("AppBundle:Colaborador");

        if ($q == null) {
            $colaboradors = $repoc->findAll();
        } else {
            $colaboradors = $repoc->search($q);
        }

        return $colaboradors;
    }

    /**
     * @Route("/deleteimg/{id}", name="delete_image_ajax")
     * @Method("POST")
     */
    public function deleteImageAction(Personal $entity) {
        $em = $this->getDoctrine()->getManager();

        $entity->setFoto(null);
        $em->flush();

        $response = new Response(json_encode(array("message" => "Imatge eliminada")));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/deletecv/{id}", name="delete_cv_ajax")
     * @Method("POST")
     */
    public function deleteCVAction(Personal $entity) {
        $em = $this->getDoctrine()->getManager();

        $entity->setCurriculum(null);
        $em->flush();

        $response = new Response(json_encode(array("message" => "CV eliminat")));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/deletecvabbr/{id}", name="delete_cvabbr_ajax")
     * @Method("POST")
     */
    public function deleteCVAAbbreviatedction(Personal $entity) {
        $em = $this->getDoctrine()->getManager();

        $entity->setCvbreu(null);
        $em->flush();

        $response = new Response(json_encode(array("message" => "CV abreviat eliminat")));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

}
