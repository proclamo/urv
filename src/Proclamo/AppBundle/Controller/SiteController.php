<?php

namespace Proclamo\AppBundle\Controller;

use Proclamo\AppBundle\Lib\PublicacionsHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Description of SiteController
 *
 * @author cristianmartin
 */
class SiteController extends Controller {

  /**
   * @Route("/{_locale}", name="home", requirements={ "_locale" = "ca|es|en" })
   * @Method("GET")
   */
  public function indexAction(Request $request, $_locale = "ca") {
    $request->setLocale($_locale);
    return $this->render('Site/index.html.twig', array(
                '_locale' => $_locale
    ));
  }

  /**
   * @Route("/pagina/{_locale}/{slug}", name="pagina_show", requirements={ "_locale" = "ca|es|en" }, defaults={"_locale=ca"})
   * @Method("GET")
   */
  public function paginaAction(Request $request, $_locale, $slug) {
    $em = $this->getDoctrine()->getManager();

    $pagina = $em->getRepository("AppBundle:Pagina")->findOneBySlug($slug, $_locale);

    if (!$pagina) {
      throw new NotFoundHttpException();
    }

    $idiomas = array();
    $locales = array("ca", "es", "en");
    $router = $this->get('router');

    foreach ($locales as $locale) {
      $pagina->setLocale($locale);
      $em->refresh($pagina);
      $ruta = $router->generate('pagina_show', array('slug' => $pagina->getSlug(), '_locale' => $locale));
      array_push($idiomas, array("ruta" => $ruta, "lang" => $locale));
    }

    $pagina->setLocale($_locale);
    $em->refresh($pagina);

    return $this->render('Site/pagina.html.twig', array(
                'entity' => $pagina,
                '_locale' => $_locale,
                'idiomas' => $idiomas
    ));
  }

  /**
   * @Route("/{_locale}/memoria/{page}", name = "memoria", requirements={ "_locale" = "ca|es|en", "page" = "\d+" }, defaults={"_locale=ca", "page" = 1})
   * @Method("GET")
   */
  public function resumsAction($page, $_locale = 'ca') {
    $paginator = $this->get('knp_paginator');
    $em = $this->getDoctrine()->getManager();

    $projectes = $em->getRepository("AppBundle:Projecte")->getResums();
    $publicacions = $em->getRepository("AppBundle:Publicacio")->getAllResums($_locale);

//        $this->get("ladybug")->log($projectes);
//        $this->get("ladybug")->log($publicacions);

    $items = array_merge($projectes, $publicacions);

    usort($items, function($item1, $item2) {
      $a = get_class($item1) == 'Proclamo\AppBundle\Entity\Projecte' ? $item1->getInici() : $item1->getAny();
      $b = get_class($item2) == 'Proclamo\AppBundle\Entity\Projecte' ? $item2->getInici() : $item2->getAny();

      return $b - $a;
    });


    $result = array();
    $anyAnterior = "";
    $retorn = array();

    foreach ($items as $item) {
      $any = get_class($item) == 'Proclamo\AppBundle\Entity\Projecte' ? $item->getInici() : $item->getAny();

      if ($any !== $anyAnterior) {
        $anyAnterior = $any;
        $retorn[$any] = array();
      }

      array_push($retorn[$any], $item);
    }

    foreach ($retorn as $any => $itemsAny) {
      usort($itemsAny, function($item1, $item2) {
        return get_class($item1) == 'Proclamo\AppBundle\Entity\Projecte' ? -1 : 1;
      });

      $grups = array();

      foreach ($itemsAny as $itemAny) {
        if (get_class($itemAny) == 'Proclamo\AppBundle\Entity\Projecte') {

          if (!key_exists('projectes', $grups)) {
            $grups['projectes'] = array();
          }

          array_push($grups['projectes'], $itemAny);
        } else {

          if (!key_exists($itemAny->getGrup(), $grups)) {
            $grups[$itemAny->getGrup()] = array();
          }

          array_push($grups[$itemAny->getGrup()], $itemAny);
        }
      }

      $pattern = PublicacionsHelper::getPattern($_locale);

      uksort($grups, function($a, $b) use ($pattern) {
        $x = array_search(strtolower($a), $pattern);
        $y = array_search(strtolower($b), $pattern);
        return $x <= $y ? -1 : 1;
      });

      array_push($result, array(
          'any' => $any,
          'grups' => $grups
      ));
    }

    $pagination = $paginator->paginate($result, $page, 10, array('tipus' => 'All'));

    return $this->render('Site/memoria.html.twig', array(
                'anys' => $pagination,
                '_locale' => $_locale,
                'page' => $page
    ));
  }

  /**
   * @Route("/{_locale}/memoria/projectes/{page}", name = "memoria_projectes", requirements={ "_locale" = "ca|es|en", "page" = "\d+" }, defaults={"_locale=ca", "page" = 1})
   * @Method("GET")
   */
  public function resumsProjectesAction($page, $_locale = 'ca') {
    $paginator = $this->get('knp_paginator');
    $em = $this->getDoctrine()->getManager();

    $items = $em->getRepository("AppBundle:Projecte")->getResums();

    $result = array();
    $retorn = array();

    foreach ($items as $item) {

      if (!key_exists($item->getInici(), $result)) {
        $result[$item->getInici()] = array();
      }

      array_push($result[$item->getInici()], $item);
    }

    foreach ($result as $any => $items) {
      array_push($retorn, array(
          'any' => $any,
          'items' => $items
      ));
    }

    $pagination = $paginator->paginate($retorn, $page, 10, array('tipus' => 'Publicacions'));

    return $this->render('Site/memoria.projectes.html.twig', array(
                'anys' => $pagination,
                '_locale' => $_locale,
                'page' => $page
    ));
  }

  /**
   * @Route("/{_locale}/memoria/llibres/{page}", name = "memoria_llibres", requirements={ "_locale" = "ca|es|en", "page" = "\d+" }, defaults={"_locale=ca", "page" = 1})
   * @Method("GET")
   */
  public function resumsLlibresAction($page, $_locale = 'ca') {
    $paginator = $this->get('knp_paginator');
    $em = $this->getDoctrine()->getManager();

    $items = $em->getRepository("AppBundle:Publicacio")->getResums('Llibres', $_locale);

//    $items = array_filter($publicacions, function($item) {
//      return $item->getGrup() === 'Llibres';
//    });

    $result = array();
    $retorn = array();

    foreach ($items as $item) {

      if (!key_exists($item->getAny(), $result)) {
        $result[$item->getAny()] = array();
      }

      array_push($result[$item->getAny()], $item);
    }

    foreach ($result as $any => $items) {
      array_push($retorn, array(
          'any' => $any,
          'items' => $items
      ));
    }

    $pagination = $paginator->paginate($retorn, $page, 10, array('tipus' => 'Publicacions'));

    return $this->render('Site/memoria.llibres.html.twig', array(
                'anys' => $pagination,
                '_locale' => $_locale,
                'page' => $page
    ));
  }

  /**
   * @Route("/{_locale}/memoria/revistes/{page}", name = "memoria_revistes", requirements={ "_locale" = "ca|es|en", "page" = "\d+" }, defaults={"_locale=ca", "page" = 1})
   * @Method("GET")
   */
  public function resumsRevistesAction($page, $_locale = 'ca') {
    $paginator = $this->get('knp_paginator');
    $em = $this->getDoctrine()->getManager();

    $items = $em->getRepository("AppBundle:Publicacio")->getResums('Revistes', $_locale);

    $result = array();
    $retorn = array();

    foreach ($items as $item) {

      if (!key_exists($item->getAny(), $result)) {
        $result[$item->getAny()] = array();
      }

      array_push($result[$item->getAny()], $item);
    }

    foreach ($result as $any => $items) {
      array_push($retorn, array(
          'any' => $any,
          'items' => $items
      ));
    }

    $pagination = $paginator->paginate($retorn, $page, 10, array('tipus' => 'Publicacions'));

    return $this->render('Site/memoria.revistes.html.twig', array(
                'anys' => $pagination,
                '_locale' => $_locale,
                'page' => $page
    ));
  }

  /**
   * @Route("/{_locale}/buscar", name="search", defaults={"page" = 1})
   * @Method("GET")
   */
  public function searchAction($page, $_locale = "ca") {
    $entities = array();

    $r = $this->getRequest();

    if ($r->query->has('q') && $r->query->get('q') != "") {

      $results = $this->get('search')->search($r->query->get('q'));

      foreach ($results as $result) {
        foreach ($result->getResults() as $res) {
          array_push($entities, $res);
        }
      }

      $paginator = $this->get('knp_paginator');

      $entities = $paginator->paginate($entities, $page, 10);
    } else {
      return $this->redirect($this->generateUrl("home"));
    }

    return $this->render('Site/search.html.twig', array(
                'entities' => $entities,
                'page' => $page,
                '_locale' => $_locale
    ));
  }

}
