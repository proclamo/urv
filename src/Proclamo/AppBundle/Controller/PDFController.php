<?php

namespace Proclamo\AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Proclamo\AppBundle\Entity\PDF;
use Proclamo\AppBundle\Form\PDFType;

/**
 * PDF controller.
 *
 * @Route("/pdf")
 */
class PDFController extends Controller
{

    /**
     * Lists all PDF entities.
     *
     * @Route("/", name="pdf")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:PDF')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new PDF entity.
     *
     * @Route("/", name="pdf_create")
     * @Method("POST")
     * @Template("AppBundle:PDF:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new PDF();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('pdf_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a PDF entity.
     *
     * @param PDF $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(PDF $entity)
    {
        $form = $this->createForm(new PDFType(), $entity, array(
            'action' => $this->generateUrl('pdf_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new PDF entity.
     *
     * @Route("/new", name="pdf_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new PDF();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a PDF entity.
     *
     * @Route("/{id}", name="pdf_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:PDF')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PDF entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing PDF entity.
     *
     * @Route("/{id}/edit", name="pdf_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:PDF')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PDF entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a PDF entity.
    *
    * @param PDF $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(PDF $entity)
    {
        $form = $this->createForm(new PDFType(), $entity, array(
            'action' => $this->generateUrl('pdf_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing PDF entity.
     *
     * @Route("/{id}", name="pdf_update")
     * @Method("PUT")
     * @Template("AppBundle:PDF:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:PDF')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PDF entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('pdf_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a PDF entity.
     *
     * @Route("/{id}", name="pdf_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:PDF')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find PDF entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('pdf'));
    }

    /**
     * Creates a form to delete a PDF entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('pdf_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
