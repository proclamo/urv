<?php

namespace Proclamo\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Proclamo\AppBundle\Entity\Noticia;
use Proclamo\AppBundle\Form\NoticiaType;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/noticies")
 */
class NoticiaController extends Controller {

    /**
     * @Route("/", name="noticia_index")
     * @Method("GET")
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Noticia')->findAll();

        return $this->render('Noticia/index.html.twig', array(
                    'entities' => $entities,
                    'title' => 'Notícies'
        ));
    }

    /**
     * @Route("/resum", name="noticia_resum")
     * @Method("GET")
     */
    public function resumAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Noticia')->findAll(2);

        return $this->render('Noticia/resum.html.twig', array(
                    'entities' => $entities
        ));
    }

    /**
     * @Route("/{slug}", name = "noticia_show")
     * @Method("GET")
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Noticia')->findOneBySlug($slug);

        return $this->render('Noticia/show.html.twig', array(
                    'entity' => $entity
        ));
    }

    /**
     * @Route("/admin/new", name = "noticia_new")
     * @Method("GET")
     */
    public function newAction() {
        $entity = new Noticia();
        $form = $this->createForm(new NoticiaType(), $entity);

        return $this->render('Noticia/new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView()
        ));
    }

    /**
     * @Route("/admin/create", name = "noticia_create")
     * @Method("POST")
     */
    public function createAction(Request $request) {
        $entity = new Noticia();
        $form = $this->createForm(new NoticiaType(), $entity);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $entity->setData(new \DateTime());

            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('noticia_index'));
        }

        return $this->render('Noticia/new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView()
        ));
    }

    /**
     * @Route("/admin/edit/{id}", name = "noticia_edit")
     * @Method("GET")
     */
    public function editAction(Noticia $entity) {
        $form = $this->createForm(new NoticiaType(), $entity);

        return $this->render('Noticia/edit.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView()
        ));
    }

    /**
     * @Route("/admin/update/{id}", name = "noticia_update")
     * @Method("POST")
     */
    public function updateAction(Noticia $entity, Request $request) {

        $form = $this->createForm(new NoticiaType(), $entity);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('noticia_show', array('slug' => $entity->getSlug())));
        }

        return $this->render('Noticia/edit.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView()
        ));
    }

    /**
     * @Route("/admin/{id}", name="noticia_delete")
     * @Method("GET")
     */
    public function deleteAction(Noticia $entity) {
        $em = $this->getDoctrine()->getManager();

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('noticia_index'));
    }
}
