<?php

namespace Proclamo\AppBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Proclamo\AppBundle\Entity\Colaborador;
use Proclamo\AppBundle\Entity\Personal;
use Proclamo\AppBundle\Entity\Publicacio;
use Proclamo\AppBundle\Entity\PublicacioAutors;
use Proclamo\AppBundle\Form\PublicacioType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Publicacio controller.
 *
 * @Route("/publicacio")
 */
class PublicacioController extends Controller {

    private $entity;
    private $membres;

    /**
     * Displays a form to create a new Publicacio entity.
     *
     * @Route("/new/{id}", name="publicacio_new")
     * @Method("GET")
     */
    public function newAction(Personal $personal) {
        $entity = new Publicacio();
        $pa = new PublicacioAutors();
        $pa->setPublicacio($entity);
        $pa->setPersonal($personal);
        $entity->addAutor($pa);

        $form = $this->createForm(new PublicacioType(), $entity);

        return $this->render('Publicacio/new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'personal' => $personal
        ));
    }

    /**
     * Creates a new Publicacio entity.
     *
     * @Route("/admin/{id}", name="publicacio_create")
     * @Method("POST")
     */
    public function createAction(Request $request, Personal $personal) {
        $this->entity = new Publicacio();
        $publicacioType = new PublicacioType();
        $form = $this->createForm($publicacioType, $this->entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $this->entity->setOriginalgrup($this->entity->getGrup());
            
            if ($request->get("membres") != null) {
                $this->membres = new ArrayCollection($request->get("membres"));
                $this->addMembres($em);
            }

            $em->persist($this->entity);
            $em->flush();
            
            return $this->redirect($this->generateUrl('personal_show', array('id' => $personal->getId())));
        }

        return $this->render('Publicacio/new.html.twig', array(
                    'entity' => $this->entity,
                    'form' => $form->createView(),
                    'personal' => $personal
        ));
    }

    private function addMembres($em) {
        foreach ($this->membres as $ordre => $membre) {
            
            if ($membre === "") {
                continue;
            }

            $pa = new PublicacioAutors();
            $pa->setPublicacio($this->entity);
            $pa->setOrdre($ordre);

            $personal = $em->getRepository('AppBundle:Personal')->findByNomCognoms($membre);
            if ($personal) {
                $pa->setPersonal($personal);
            }

            $colaborador = $em->getRepository('AppBundle:Colaborador')->findByNomCognoms($membre);
            if ($colaborador) {
                $pa->setColaborador($colaborador);
            }
            
            if (!$personal && !$colaborador) {
                // crear colaborador
                $colaborador = new Colaborador();
                $colaborador->setNom($membre);                
                $this->getDoctrine()->getManager()->persist($colaborador);
                $pa->setColaborador($colaborador);
            }

            $this->entity->addAutor($pa);
        }
    }

    private function removeMembres($em) {

        $autors = $em->getRepository('AppBundle:PublicacioAutors')->findByPublicacio($this->entity->getId());

        foreach ($autors as $autor) {
            $em->remove($autor);
        }
    }

    /**
     * Displays a form to edit an existing Publicacio entity.
     *
     * @Route("/admin/{_locale}/{personal}/{id}/edit", name="publicacio_edit", requirements={ "_locale" = "ca|es|en" })
     * @Method("GET")
     */
    public function editAction($_locale, Personal $personal, Publicacio $entity) {
        $em = $this->getDoctrine()->getManager();

        $editForm = $this->createForm(new PublicacioType(), $entity);

        return $this->render('Publicacio/edit.html.twig', array(
                    'entity' => $entity,
                    'form' => $editForm->createView(),
                    '_locale' => $_locale,
                    'personal' => $personal
        ));
    }

    /**
     * Edits an existing Publicacio entity.
     *
     * @Route("/admin/{_locale}/{personal}/{id}", name="publicacio_update", requirements={ "_locale" = "ca|es|en" })
     * @Method("POST")
     */
    public function updateAction(Request $request, Personal $personal, Publicacio $entity, $_locale) {
        $em = $this->getDoctrine()->getManager();

        $this->entity = $entity;

        $editForm = $this->createForm(new PublicacioType(), $this->entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            
            $originalgrup = $this->entity->getOriginalgrup();
            $grup = $this->entity->getGrup();
            
            if ($_locale == 'ca' && $grup != $originalgrup) {
              $this->entity->setOriginalgrup($grup);
            }
            
            if ($request->get("membres") != null) {
                $this->membres = new ArrayCollection($request->get("membres"));
                $this->removeMembres($em);
                $this->addMembres($em);
            }
            $em->persist($this->entity);
            $em->flush();
            
            return $this->redirect($this->generateUrl('personal_show', array('id' => $personal->getId())));
        }

        return $this->render('Publicacio/edit.html.twig', array(
                    'entity' => $this->entity,
                    'form' => $editForm->createView(),
                    '_locale' => $_locale,
                    'personal' => $personal
        ));
    }

    /**
     * Deletes a Publicacio entity.
     *
     * @Route("/admin/{personal}/{id}", name="publicacio_delete")
     * @Method("GET")
     */
    public function deleteAction(Publicacio $entity, $personal) {
        $em = $this->getDoctrine()->getManager();

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('personal_show', array('id' => $personal)));
    }

    /**
     * @Route("/publicacio/search", name = "publicacions_search_agrupacio")
     */
    public function searchAgrupacio() {
        $q = $this->getRequest()->get('term');

        $ret = $this->searchAgrupacions($q);

        $response = new Response(json_encode(array("data" => $ret)));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    private function searchAgrupacions($q) {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository("AppBundle:Publicacio");

        if ($q == null) {
            return $repository->findByGrup();
        } else {
            return $repository->searchAgrupacions($q);
        }
    }

}
