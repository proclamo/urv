<?php

namespace Proclamo\AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Proclamo\AppBundle\Entity\Docencia;
use Proclamo\AppBundle\Entity\Personal;
use Proclamo\AppBundle\Form\DocenciaType;

/**
 * Docencia controller.
 *
 * @Route("/docencia")
 */
class DocenciaController extends Controller
{

    /**
     * Lists all Docencia entities.
     *
     * @Route("/", name="docencia")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Docencia')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Docencia entity.
     *
     * @Route("/admin/{id}", name="docencia_create")
     * @Method("POST")
     */
    public function createAction(Request $request, Personal $personal)
    {
        $entity = new Docencia();
        
        $entity->setProfesor($personal);
        
        $form = $this->createForm(new DocenciaType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('personal_show', array('id' => $personal->getId())));
        }

        return $this->render('Docencia/new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'personal' => $personal
        ));
    }


    /**
     * Displays a form to create a new Docencia entity.
     *
     * @Route("/new/{id}/", name="docencia_new")
     * @Method("GET")
     */
    public function newAction(Personal $personal)
    {
        $entity = new Docencia();        
        $form = $this->createForm(new DocenciaType(), $entity);

        return $this->render('Docencia/new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'personal' => $personal
        ));
    }

    /**
     * Finds and displays a Docencia entity.
     *
     * @Route("/{id}", name="docencia_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Docencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Docencia entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Docencia entity.
     *
     * @Route("/admin/{_locale}/{personal}/{id}/edit", name="docencia_edit", requirements={ "_locale" = "ca|es|en" })
     * @Method("GET")
     */
    public function editAction($_locale, Personal $personal,Docencia $entity)
    {
        $em = $this->getDoctrine()->getManager();

        $editForm = $this->createForm(new DocenciaType(), $entity);

        return $this->render('Docencia/edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            '_locale'     => $_locale,
            'personal'    => $personal
        ));
    }

    /**
     * Edits an existing Docencia entity.
     *
     * @Route("/admin/{_locale}/{personal}/{id}", name="docencia_update", requirements={ "_locale" = "ca|es|en" })
     * @Method("POST")
     */
    public function updateAction(Request $request, $personal, Docencia $entity, $_locale)
    {
        $em = $this->getDoctrine()->getManager();

        $editForm = $this->createForm(new DocenciaType(), $entity);
        $editForm->handleRequest($request);       
        
        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('personal_show', array('id' => $personal)));
        }

        return $this->render('Docencia/edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            '_locale'     => $_locale,
            'personal'    => $personal
        ));
    }
    
    /**
     * Deletes a Docencia entity.
     *
     * @Route("/admin/{personal}/{id}", name="docencia_delete")
     * @Method("GET")
     */
    public function deleteAction(Docencia $entity, $personal)
    {        
        $em = $this->getDoctrine()->getManager();
        
        $em->remove($entity);
        $em->flush();
        
        return $this->redirect($this->generateUrl('personal_show', array('id' => $personal)));
    }

}
