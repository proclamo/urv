<?php

namespace Proclamo\AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Proclamo\AppBundle\Entity\Menu;
use Proclamo\AppBundle\Entity\Pagina;
use Proclamo\AppBundle\Form\PaginaType;

/**
 * @Route("/admin/pagines")
 */
class PaginaController extends Controller {

    /**
     * @Route("/{_locale}/", name="pagina_index", requirements={ "_locale" = "ca|es|en" })
     * @Method("GET")
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        
        $locale = $request->getLocale();
        
        $pagines = $em->getRepository('AppBundle:Pagina')->findAll($locale);
        
        $entities = $this->agrupa($pagines, "menus");

        return $this->render('Pagina/index.html.twig', array(
                    'entities' => $entities,
                    'title' => 'Totes les pàgines',
                    '_locale' => $locale
        ));
    }
    
    private function agrupa($menus, $agrupacio) {
        $agrupacions = array();

        foreach ($menus as $menu) {
            $grup = $menu->getGrup();

            if ($grup == "") {
                $grup = "Sense asignar";
            }

            if (!isset($agrupacions[$grup])) {
                $agrupacions[$grup] = array("nom" => $grup, $agrupacio => array());
            }

            array_push($agrupacions[$grup][$agrupacio], $menu);
        }

        return $agrupacions;
    }
    
    /**
     * @Route("/new", name="pagina_new")
     * @Method("GET")
     */
    public function newAction() {
        $entity = new Pagina();
        $form = $this->createForm(new PaginaType(), $entity);

        return $this->render('Pagina/new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView()
        ));
    }

    /**
     * @Route("/create", name="pagina_create")
     * @Method("POST")
     */
    public function createAction(Request $request) {
        $entity = new Pagina();
        $form = $this->createForm(new PaginaType(), $entity);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($entity);
            $em->flush();
            
            $menu = new Menu();
            $menu->setGrup($entity->getGrup());
            $menu->setNom($entity->getTitol());
            $menu->setPosicio($entity->getOrdre());
            $menu->setRuta($entity->getSlug());
            $menu->setPagina($entity->getId());
            
            $em->persist($menu);
            $em->flush();

            return $this->redirect($this->generateUrl('pagina_index'));
        }

        return $this->render('Pagina/new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView()
        ));
    }
    
    /**
     * @Route("/edit/{_locale}/{id}", name="pagina_edit", requirements={ "_locale" = "ca|es|en" })
     * @Method("GET")
     */
    public function editAction(Pagina $entity, $_locale)
    {
        $entity->setLocale($_locale);
        $form = $this->createForm(new PaginaType(), $entity);
        
        return $this->render('Pagina/edit.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            '_locale'     => $_locale
        ));
    }
    
    /**
     * @Route("/update/{_locale}/{id}", name="pagina_update", requirements={ "_locale" = "ca|es|en" })
     * @Method("POST")
     */
    public function updateAction(Request $request, Pagina $entity, $_locale)
    {
        $em = $this->getDoctrine()->getManager();
        
        $menu = $em->getRepository("AppBundle:Menu")->findOneByPagina($entity->getId());
        
        $form = $this->createForm(new PaginaType(), $entity);
        $form->handleRequest($request);
        
        if ($form->isValid()) {
                        
            $em->persist($entity);
            $em->flush();
            
            //$em->refresh($entity);
            $entity->setLocale($_locale);
            
            $menu->setGrup($entity->getGrup());
            $menu->setNom($entity->getTitol());
            $menu->setPosicio($entity->getOrdre());
            $menu->setRuta($entity->getSlug());
            $menu->setLocale($_locale);
            
            $em->persist($menu);
            $em->flush();

            return $this->redirect($this->generateUrl('pagina_index'));
        }
        
        return $this->render('Pagina/edit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            '_locale' => $_locale
        ));
    }

    /**
     * @Route("/delete/{id}", name="pagina_delete")
     * @Method("GET")
     */
    public function deleteAction(Pagina $entity)
    {
        $em = $this->getDoctrine()->getManager();
        
        $menu = $em->getRepository("AppBundle:Menu")->findOneByRuta($entity->getSlug());
        
        $em->remove($entity);
        $em->remove($menu);
        $em->flush();
        
        return $this->redirect($this->generateUrl('pagina_index'));
        
    }
    
        /**
     * @Route("/deleteimg/{id}", name="delete_image_pagina_ajax")
     * @Method("POST")
     */
    public function deleteImageAction(Pagina $entity) {
        $em = $this->getDoctrine()->getManager();

        $entity->setFoto(null);
        $em->flush();

        $response = new Response(json_encode(array("message" => "Imatge eliminada")));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

}
