<?php

namespace Proclamo\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Proclamo\AppBundle\Entity\Model;
use Proclamo\AppBundle\Form\ModelsType;


/**
 * @Route("models-administratius")
 */
class ModelsController extends Controller
{
    /**
     * @Route("/{_locale}/", name="models", requirements={ "_locale" = "ca|es|en" })
     * @Method("GET")
     */
    public function indexAction($_locale = "ca")
    {
        $em = $this->getDoctrine()->getManager();

        $models = $em->getRepository('AppBundle:Model')->findPerIdioma($_locale);
        
        $entities = $this->agrupa($models, "models");

        return $this->render('Models/index.html.twig', array(
            'entities' => $entities,
            '_locale' => $_locale,
            'title' => 'models'
        ));
        
    }
    
    private function agrupa($menus, $agrupacio) {
        $agrupacions = array();

        foreach ($menus as $menu) {
            $grup = $menu->getGrup();

            if ($grup == "") {
                $grup = "Sense asignar";
            }

            if (!isset($agrupacions[$grup])) {
                $agrupacions[$grup] = array("nom" => $grup, $agrupacio => array());
            }

            array_push($agrupacions[$grup][$agrupacio], $menu);
        }

        return $agrupacions;
    }
    
    /**
     * @Route("/new", name="model_new")
     * @Method("GET")
     */
    public function newAction() {
        $entity = new Model();
        $form = $this->createForm(new ModelsType(), $entity);

        return $this->render('Models/new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView()
        ));
    }

    /**
     * @Route("/create/{_locale}", name="model_create", requirements={ "_locale" = "ca|es|en" })
     * @Method("POST")
     */
    public function createAction(Request $request, $_locale = "ca") {
        $entity = new Model();
        $form = $this->createForm(new ModelsType(), $entity);

        $form->handleRequest($request);

        if ($form->isValid()) {
            
            $entity->setLocale($_locale);
          
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($entity);
            $em->flush();
            
            return $this->redirect($this->generateUrl('models'));
        }

        return $this->render('Models/new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView()
        ));
    }
    
    /**
     * @Route("/edit/{_locale}/{id}", name="model_edit", requirements={ "_locale" = "ca|es|en" })
     * @Method("GET")
     */
    public function editAction(Model $entity, $_locale)
    {
        $form = $this->createForm(new ModelsType(), $entity);
        
        return $this->render('Models/edit.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            '_locale'     => $_locale
        ));
    }
    
    /**
     * @Route("/update/{_locale}/{id}", name="model_update", requirements={ "_locale" = "ca|es|en" })
     * @Method("POST")
     */
    public function updateAction(Request $request, Model $entity, $_locale)
    {
        $em = $this->getDoctrine()->getManager();
        
        $form = $this->createForm(new ModelsType(), $entity);
        $form->handleRequest($request);
        
        if ($form->isValid()) {                        
            $em->persist($entity);
            $em->flush();
            
            return $this->redirect($this->generateUrl('models', array('_locale' => $_locale)));
        }
        
        return $this->render('Models/edit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            '_locale' => $_locale
        ));
    }

    /**
     * @Route("/delete/{id}", name="model_delete")
     * @Method("GET")
     */
    public function deleteAction(Model $entity)
    {
        $em = $this->getDoctrine()->getManager();
        
        $em->remove($entity);        
        $em->flush();
        
        return $this->redirect($this->generateUrl('models'));
        
    }
    
    
}
