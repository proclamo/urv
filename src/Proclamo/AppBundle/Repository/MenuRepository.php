<?php

namespace Proclamo\AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of UserRepository
 *
 * @author cristianmartin
 */
class MenuRepository extends EntityRepository 
{
    
    public function setTranslations($qb, $locale = 'ca')
    {
        // Use Translation Walker
        $query = $qb->getQuery();
        $query->setHint(
            \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
            'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
        );
        // Force the locale
        $query->setHint(
            \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
            $locale
        );
        return $query;
    }
    
    public function findByGrup($grup, $locale = 'ca') {
        
        $q = $this->createQueryBuilder('m')
                ->andWhere('m.grup = :grup')                
                ->orderBy('m.posicio')
                ->setParameter('grup', $grup);
        
        return $this->setTranslations($q, $locale)->getArrayResult();
    }
    
    public function findAllAgrupat($locale = 'ca') {
        $q = $this->createQueryBuilder('m')
                ->andWhere("m.grup not in ('departament')")
                ->addOrderBy('m.grup')
                ->addOrderBy('m.posicio');
        
        return $this->setTranslations($q, $locale)->getArrayResult();
    }
}
