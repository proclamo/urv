<?php

namespace Proclamo\AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of PaginaRepository
 *
 * @author cristianmartin
 */
class PaginaRepository extends EntityRepository {
    
    public function queryAll()
    {
        $qb = $this->createQueryBuilder('a');
        $qb->select('a');
        $qb->addOrderBy('a.ordre');
        
        return $qb;
    }
    
    public function setTranslations($qb, $locale = 'ca')
    {
        // Use Translation Walker
        $query = $qb->getQuery();
        $query->setHint(
            \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
            'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
        );
        // Force the locale
        $query->setHint(
            \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
            $locale
        );
        return $query->getResult();
    }
    
    public function findAll($locale = 'ca')
    {
        $qb = $this->queryAll();        
        return $this->setTranslations($qb, $locale);
    }
    
    public function findAllByGrup($grup, $locale = 'ca')
    {
        $qb = $this->queryAll();  
        $qb->andWhere('a.grup = :grup')
            ->setParameter('grup', $grup);
        
        return $this->setTranslations($qb, $locale);
    }
    
    public function findOneBySlug($slug, $locale)
    {
        $qb = $this->createQueryBuilder('a')
                ->andWhere('a.slug = :slug')
                ->setParameter('slug', $slug)
                ;
        
        $result = $this->setTranslations($qb, $locale);
        
        return $result[0];
    }
    
    public function findOneById($id, $locale)
    {
        $qb = $this->createQueryBuilder('a')
                ->andWhere('a.id = :id')
                ->setParameter('id', $id)
                ;
        
        $result = $this->setTranslations($qb, $locale);
        
        return $result[0];
    }
//    
//    public function buscaPorTitulo($q)
//    {
//        return $this->getEntityManager()
//                ->createQueryBuilder()
//                ->select('DISTINCT p.titol, p.slug')
//                ->from('AppBundle:Pagina', 'p')
//                ->andWhere('p.titol LIKE :term')
//                ->setParameter('term', $q . '%')
//                ->getQuery()
//                ->execute();
//                ;
//    }
}
