<?php

namespace Proclamo\AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of NoticiaRepository
 *
 * @author cristianmartin
 */
class NoticiaRepository extends EntityRepository {

    public function queryAll() {
        $qb = $this->createQueryBuilder('a');
        $qb->select('a');

        return $qb;
    }

    public function findAll($limit = 0) {
        $qb = $this->queryAll()
                ->orderBy('a.data', 'DESC');

        if ($limit > 0) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

}
