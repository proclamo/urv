<?php

namespace Proclamo\AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of EventRepository
 *
 * @author cristianmartin
 */
class EventRepository extends EntityRepository {

    public function queryAll() {
        $qb = $this->createQueryBuilder('a');
        $qb->select('a');

        return $qb;
    }
    
    public function findFutures($limit = 0) {
        
        $now = new \DateTime('NOW');
        
        $qb = $this->queryAll()
                ->andWhere('a.data >= :now')
                ->orderBy('a.data', 'ASC')
                ->setParameter('now', $now);
        
        if ($limit > 0) {
            $qb->setMaxResults($limit);
        }
        
        return $qb->getQuery()->getResult();
    }
    
    public function findPassats() {
        
        $now = new \DateTime('NOW');
        
        $qb = $this->queryAll()
                ->andWhere('a.data < :now')
                ->orderBy('a.data', 'DESC')
                ->setParameter('now', $now);
        
        return $qb->getQuery()->getResult();
    }

}
