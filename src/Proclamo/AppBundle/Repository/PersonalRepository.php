<?php

namespace Proclamo\AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of PersonalRepository
 *
 * @author cristianmartin
 */
class PersonalRepository  extends EntityRepository {

    public function queryAll()
    {
        $qb = $this->createQueryBuilder('a');
        $qb->select('a')
           ->addOrderBy("a.nomCognoms");

        return $qb;
    }

    public function setTranslations($qb, $locale = 'ca')
    {
        // Use Translation Walker
        $query = $qb->getQuery();
        $query->setHint(
            \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
            'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
        );
        // Force the locale
        $query->setHint(
            \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
            $locale
        );
        return $query->getResult();
    }

    public function findAll($locale = 'ca')
    {
        $qb = $this->queryAll();
        return $this->setTranslations($qb, $locale);
    }
    
    public function findActius($locale = 'ca')
    {
        return $this->queryAll($locale)
                ->andWhere('a.dataBaixa is null');
    }
    
    public function findByTipusJornada($tipus, $jornada = null, $locale = 'ca')
    {
        $qb = $this->findActius()
                ->andWhere("a.tipus IN (:tipus)");

        if ($jornada != null) {
            $qb->andWhere("a.jornada = :jornada")
            ->setParameter("jornada", $jornada);
        }

        $qb->setParameter("tipus", $tipus);

        return $this->setTranslations($qb, $locale);
    }

    public function findBySituacio($situacio, $locale = 'ca')
    {
        $qb = $this->findActius()
                ->andWhere("a.situacio IN (:situacio)")
                ->setParameter("situacio", $situacio);

        return $this->setTranslations($qb, $locale);
    }

    public function search($nom)
    {
        $query = $this->createQueryBuilder('p')
                ->andWhere('lower(p.nomCognoms) LIKE :nom')
                ->setParameter('nom', '%' . strtolower($nom) . '%')
                ->getQuery();

        return $query->execute();
    }

    public function findByNomCognoms($nomCognoms)
    {
        $query = $this->createQueryBuilder('p')
                ->andWhere('p.nomCognoms = :nom')
                ->setParameter('nom', $nomCognoms)
                ->getQuery();

        return $query->getOneOrNullResult();
    }
}
