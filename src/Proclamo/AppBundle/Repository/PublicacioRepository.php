<?php

namespace Proclamo\AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of PublicacioRepository
 *
 * @author cristianmartin
 */
class PublicacioRepository extends EntityRepository {

    public function queryAll() {
        $qb = $this->createQueryBuilder('a');
        $qb->select('a');

        return $qb;
    }

    public function setTranslations($qb, $locale = 'ca') {
        // Use Translation Walker
        $query = $qb->getQuery();
        $query->setHint(
                \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
        );
        // Force the locale
        $query->setHint(
                \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale
        );
        return $query->getResult();
    }

    public function findAll($locale = 'ca') {
        $qb = $this->queryAll();
        return $this->setTranslations($qb, $locale);
    }

    public function getFromPersonal($id, $locale = 'ca') {
        $qb = $this->createQueryBuilder('p')
                ->join('p.autors', 'a')
                ->andWhere('a.personal in (:id)')
                ->orderBy('p.any', 'DESC')
                ->setParameter('id', $id);

        return $this->setTranslations($qb, $locale);
    }
    
    public function getFromColaborador($id, $locale = 'ca') {
        $qb = $this->createQueryBuilder('p')
                ->join('p.autors', 'a')
                ->orWhere('a.colaborador in (:id)')
                ->setParameter('id', $id);

        return $this->setTranslations($qb, $locale);
    }

    public function searchAgrupacions($q) {
        return $this->getEntityManager()
                        ->createQueryBuilder()
                        ->select('DISTINCT p.grup')
                        ->from('AppBundle:Publicacio', 'p')
                        ->andWhere('p.grup LIKE :term')
                        ->setParameter('term', $q . '%')
                        ->getQuery()
                        ->execute();
        ;
    }
    
    public function getAllResums($locale = 'ca')
    {
        $qb = $this->queryAll()
                ->where('a.id in (select distinct(pa.publicacio) from AppBundle:PublicacioAutors pa where pa.personal is not null)')
                ->addOrderBy('a.any', 'DESC')
                ->addOrderBy('a.grup', 'DESC')
                ;

        return $this->setTranslations($qb, $locale);
    }
    
    public function getResums($grup, $locale = 'ca')
    {
        $qb = $this->queryAll()
                ->where('a.id in (select distinct(pa.publicacio) from AppBundle:PublicacioAutors pa where pa.personal is not null)')
                ->andWhere('a.originalgrup=:grup')
                ->addOrderBy('a.any', 'DESC')
                ->addOrderBy('a.grup', 'DESC')
                ->setParameter('grup', $grup)
                ;

        return $this->setTranslations($qb, $locale);
    }
    
    public function search($q)
    {
        $query = $this->createQueryBuilder('p')
                ->join('p.autors', 'a')
                ->andWhere('lower(p.titol) LIKE :q')
                ->orWhere('lower(p.grup) LIKE :q')
                ->orWhere('lower(p.nomMedi) LIKE :q')
                ->setParameter('q', '%' . strtolower($q) . '%')
                ->getQuery();

        return $query->execute();
    }
}
