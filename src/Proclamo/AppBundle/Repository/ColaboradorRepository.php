<?php

namespace Proclamo\AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of ColaboradorRepository
 *
 * @author cristianmartin
 */
class ColaboradorRepository extends EntityRepository {
    
    public function search($nom)
    {
        $query = $this->createQueryBuilder('c')
                ->andWhere('c.nom LIKE :nom')
                ->setParameter('nom', $nom . '%')
                ->getQuery();
        
        return $query->execute();
    }
    
    public function findByNomCognoms($nomCognoms)
    {
        $query = $this->createQueryBuilder('c')
                ->andWhere('c.nom = :nom')
                ->setParameter('nom', $nomCognoms)
                ->getQuery();
        
        return $query->getOneOrNullResult();
    }
}
