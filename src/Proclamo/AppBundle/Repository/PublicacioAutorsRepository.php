<?php

namespace Proclamo\AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of PublicacioAutorsRepository
 *
 * @author cristianmartin
 */
class PublicacioAutorsRepository extends EntityRepository {
    
    public function findByPublicacio($publicacioId)
    {
        return $this->getEntityManager()
                ->createQuery('Select a from AppBundle:PublicacioAutors a where a.publicacio = :id order by a.ordre')
                ->setParameter("id", $publicacioId)
                ->getResult();
    }
    
    public function findMembres($publicacioId)
    {
        $membres = array();
        
        $publicacions = $this->findByPublicacio($publicacioId);
        
        foreach ($publicacions as $publicacio) {
            
            if ($publicacio->getPersonal() != null) {
                array_push($membres, $publicacio->getPersonal());
            }
            
            if ($publicacio->getColaborador() != null) {
                array_push($membres, $publicacio->getColaborador());
            }
        }
        
        return $membres;
    }
}
