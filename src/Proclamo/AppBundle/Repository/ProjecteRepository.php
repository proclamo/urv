<?php

namespace Proclamo\AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of ProjecteRepository
 *
 * @author cristianmartin
 */
class ProjecteRepository extends EntityRepository {

    public function getFromPersonalInvestigadorPrincipal($id) {
        return $this->getEntityManager()
                        ->createQuery('select p from AppBundle:Projecte p where p.investigador = :id')
                        ->setParameter('id', $id)
                        ->getResult()
        ;
    }

    public function getFromPersonal($id) {
        $result1 = $this->getFromPersonalInvestigadorPrincipal($id);
        

        $result2 = $this->getEntityManager()
                ->createQuery('select p from AppBundle:Projecte p join p.autors c where c.id = :id')
                ->setParameter('id', $id)
                ->getResult()
        ;


        $results = array_merge($result1, $result2);
        
        usort($results, function($a, $b) {
          if ($a->getInici() == $b->getInici()) {
            return $a->getFinal() <= $b->getFinal() ? 1 : - 1;
          }
          else {
            return $a->getInici() <= $b->getInici() ? 1 : -1;
          }
        });
        
        return $results;
    }

    public function searchAgrupacions($q) {
        return $this->getEntityManager()
                        ->createQueryBuilder()
                        ->select('DISTINCT p.grup')
                        ->from('AppBundle:Projecte', 'p')
                        ->andWhere('p.grup LIKE :term')
                        ->setParameter('term', $q . '%')
                        ->getQuery()
                        ->execute();
        ;
    }

    public function getResums()
    {
        return $this->getEntityManager()
                ->createQueryBuilder('p')
                ->select('p')
                ->from('AppBundle:Projecte', 'p')
                ->addOrderBy('p.inici', 'DESC')
                ->getQuery()
                ->execute();
    }

    public function search($q)
    {
        $query = $this->createQueryBuilder('p')
                ->join('p.investigador', 'i')
                ->andWhere('lower(p.titol) LIKE :q')
                ->orWhere('lower(p.descripcio) LIKE :q')
                ->orWhere('lower(p.codi) LIKE :q')
                ->orWhere('lower(i.nomCognoms) LIKE :q')
                ->setParameter('q', '%' . strtolower($q) . '%')
                ->getQuery();

        return $query->execute();
    }
}
