<?php

namespace Proclamo\AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of DocenciaRepository
 *
 * @author cristianmartin
 */
class DocenciaRepository extends EntityRepository {
    
    public function queryAll()
    {
        $qb = $this->createQueryBuilder('a');
        $qb->select('a');
        
        return $qb;
    }
    
    public function setTranslations($qb, $locale = 'ca')
    {
        // Use Translation Walker
        $query = $qb->getQuery();
        $query->setHint(
            \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
            'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
        );
        // Force the locale
        $query->setHint(
            \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
            $locale
        );
        return $query->getResult();
    }
    
    public function findAll($locale = 'ca')
    {
        $qb = $this->queryAll();        
        return $this->setTranslations($qb, $locale);
    }
    
    public function getFromPersonal($nom, $locale = 'ca')
    {
        $qb = $this->createQueryBuilder('d')
                ->select(array('p','d'))                
                ->join('d.profesor','p')
                ->andWhere('p.nomCognoms = :nom')
                ->setParameter('nom', $nom);
        
        return $this->setTranslations($qb, $locale);
    }
}
