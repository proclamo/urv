<?php

namespace Proclamo\AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ModelRepository extends EntityRepository
{
    public function findAll() {
        return $this->createQueryBuilder('m')
                ->select('m')
                ->addOrderBy('m.posicio', 'ASC')
                ->getQuery()
                ->execute();
    }
    
    public function findPerIdioma($locale = "ca") {
      return $this->createQueryBuilder('m')
                ->select('m')
                ->addOrderBy('m.posicio', 'ASC')
                ->where("m.locale = :locale")
                ->setParameter("locale", $locale)
                ->getQuery()
                ->execute();
    }
            
}
