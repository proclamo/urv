<?php

namespace Proclamo\AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\RouterInterface;

/**
 * Description of MenuBuilder
 *
 * @author cristianmartin
 */
class MenuBuilder
{
    private $factory;
    private $em;
    private $router;
    private $locale;
    
    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory, EntityManager $entityManager, RouterInterface $router)
    {
        $this->factory = $factory;
        $this->em = $entityManager;
        $this->router = $router;
    }

    public function createMainMenu(Request $request)
    {
        $menu = $this->factory->createItem('main');

        $menu->addChild('Departament', array('route' => 'home'))->setExtra('translation_domain', 'menu');
        
        $this->locale = $request->getLocale();
        
        $this->createDepartamentMenu('departament', $menu["Departament"]);
        
        $menu->addChild('Personal', array('route' => 'personal'))->setExtra('translation_domain', 'menu');
        
        $this->createPersonalMenu($menu["Personal"]);
        
        $menu->addChild('Estudis de grau', array('route' => 'home'))->setExtra('translation_domain', 'menu');
        
        $this->createSubmenu('grau', $menu['Estudis de grau']);
        
        $menu->addChild('Estudis de postgrau', array('route' => 'home'))->setExtra('translation_domain', 'menu');
        
        $this->createSubmenu('postgrau', $menu['Estudis de postgrau']);
        
        $menu->addChild('Recerca', array('route' => 'home'))->setExtra('translation_domain', 'menu');
        
        $this->createRecercaMenu($menu['Recerca']);

        return $menu;
    }
    
    public function createPersonalMenu($menu)
    {        
        $menu->addChild('Professorat temps complet', array('route' => 'personal_professor_tc'))->setExtra('translation_domain', 'menu');
        $menu->addChild('Professorat temps parcial', array('route' => 'personal_professor_tp'))->setExtra('translation_domain', 'menu');
        $menu->addChild('Becaris', array('route' => 'personal_becaris'))->setExtra('translation_domain', 'menu');
        $menu->addChild('Pas', array('route' => 'personal_pas'))->setExtra('translation_domain', 'menu');        
        $menu->addChild('Tots', array('route' => 'personal'))->setExtra('translation_domain', 'menu');
        
        return $menu;
    }
    
    public function createDepartamentMenu($nom, $menu)
    {
        $items = $this->em->getRepository('AppBundle:Pagina')->findAllByGrup("departament", $this->locale);
        
        foreach ($items as $item) {
            $ruta = $this->router->generate('pagina_show', array('slug' => $item->getSlug()));
            $menu->addChild($item->getTitol(), array('uri' => $ruta))->setExtra('translation_domain', 'menu');
        }
        
        $ruta = $this->router->generate('models', array("_locale" => $this->locale));
        $menu->addChild('models', array('uri' => $ruta))->setExtra('translation_domain', 'menu');
        
        return $menu;
    }
    
    public function createRecercaMenu($menu) {
        
        $ruta = $this->router->generate('memoria', array("_locale" => $this->locale));
        $menu->addChild('memoria', array('uri' => $ruta))->setExtra('translation_domain', 'menu');
        
        $ruta = $this->router->generate('memoria_projectes', array("_locale" => $this->locale));
        $menu->addChild('personal.projectes', array('uri' => $ruta))->setExtra('translation_domain', 'app');
        
        $ruta = $this->router->generate('memoria_llibres', array("_locale" => $this->locale));
        $menu->addChild('llibres', array('uri' => $ruta))->setExtra('translation_domain', 'menu');
        
        $ruta = $this->router->generate('memoria_revistes', array("_locale" => $this->locale));
        $menu->addChild('revistes', array('uri' => $ruta))->setExtra('translation_domain', 'menu');
        
        return $this->createSubmenu('recerca', $menu);
    }
    
    public function createSubmenu($nom, $menu)
    {
        $items = $this->em->getRepository('AppBundle:Pagina')->findAllByGrup($nom, $this->locale);
        
        foreach ($items as $item) {
            $ruta = $this->router->generate('pagina_show', array('slug' => $item->getSlug(), "_locale" => $this->locale));
            $menu->addChild($item->getTitol(), array('uri' => $ruta))->setExtra('translation_domain', 'menu');
        }
        
        return $this->createMenuFromDB($nom, $menu);
        
    }
    
    
    public function createMenuFromDB($nom, $menu)
    {
        $items = $this->em->getRepository('AppBundle:Menu')->findByGrup($nom, $this->locale);
        
        foreach ($items as $item) {
            if (!$item["pagina"]) {
                $menu->addChild($item["nom"], array('uri' => $item["ruta"]))->setExtra('translation_domain', 'menu');
            }
        }
        
        return $menu;
    }
   
}