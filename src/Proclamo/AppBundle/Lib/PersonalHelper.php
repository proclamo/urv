<?php

namespace Proclamo\AppBundle\Lib;

/**
 * Description of PersonalHelper
 *
 * @author cristianmartin
 */
class PersonalHelper {

    public function getCategoriaProfesional() {
        return array(
            'catedràtic' => "catedràtic/a",
            'catedràtic_universitat' => "catedràtic/a d'universitat",
            "titular_universitat" => "titular d'universitat",
            "titular_escola_universitaria" => "titular d'escola universitària",
            "agregat" => "agregat/da",
            "ajudant" => "ajudant",
            "collaborador_permanent" => "col·laborador/a permanent",
            "lector" => "lector/a",
            "visitant" => "visitant",
            "investigador_SECTI" => "investigador/a SECTI",
            "associat" => "associat/da",
            "responsable_administratiu" => "responsable administratiu/va",
            "administratiu" => "administratiu/va",
            "tècnic_informàtic" => "tècnic/a d'informàtica",
            "personal_suport_recerca" => "personal de suport a la recerca",
            "becari_predoctoral" => "becari/ària predoctoral",
            "becari_AGAUR" => "becari/ària AGAUR",
            "altres" => "altres"
        );
    }

    public function getSituacioAdministrativa() {
        return array(
            "funcionari" => "funcionari/ària",
            "contractat" => "contractat/a",
            "altres" => "altres"
        );
    }

    public function getDedicacio() {
        return array(
            'completa' => 'a temps complet',
            'parcial' => 'a temps parcial'
        );
    }

    public function getEspecialitat() {
        return array(
            'organitzacio_empreses' => "organització d'empreses",
            'comptabilitat' => "comptabilitat",
            'matematiques' => "matemàtiques",
            'finances' => "finances",
            'comercialitzacio' => "comercialització i investigació de mercats",
            'sociologia' => 'sociologia'
        );
    }

}
