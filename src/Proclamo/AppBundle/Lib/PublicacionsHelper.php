<?php

namespace Proclamo\AppBundle\Lib;

/**
 * Description of PublicacionsHelper
 *
 * @author cristianmartin
 */
class PublicacionsHelper {
  
  public static function getPattern($locale) {
    if ($locale == 'ca') {
      return array("projectes", "llibres", "articles", "revistes", "docencia");
    }
    if ($locale == 'es') {
      return array("proyectos", "libros", "articulos", "revistas", "docencia");
    }
    if ($locale == 'en') {
      return array("projects", "books", "peer-reviewed articles", "reviews", "teaching");
    }
  }
}
