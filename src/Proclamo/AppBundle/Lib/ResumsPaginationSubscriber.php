<?php

namespace Proclamo\AppBundle\Lib;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Knp\Component\Pager\Event\ItemsEvent;

/**
 * Description of ResumsPaginationSubscriber
 *
 * @author cristianmartin
 */
class ResumsPaginationSubscriber implements EventSubscriberInterface {

  public function items(ItemsEvent $event) {

    if (is_array($event->target)) {
      if (key_exists('tipus', $event->options) && $event->options['tipus'] == 'All') {

        // aplanamos array
        $temp = array();

        foreach ($event->target as $any) {
          foreach ($any['grups'] as $nom => $grup) {
            foreach ($grup as $item) {
              array_push($temp, array(
                  'any' => $any['any'],
                  'grup' => $nom,
                  'item' => $item
              ));
            }
          }
        }

        $items = array_slice($temp, $event->getOffset(), $event->getLimit());

        // recuperamos estructura original

        $res = array();
        $grup = array();
        $anyAnterior = '';
        $grupAnterior = '';

        foreach ($items as $item) {
          if ($item['any'] != $anyAnterior) {
            $anyAnterior = $item['any'];


            if (key_exists('grups', $grup) && count($grup['grups']) > 0) {
              array_push($res, $grup);
            }


            $grup = array(
                'any' => $anyAnterior,
                'grups' => array()
            );
            
            $grupAnterior = '';
          }

          if ($item['grup'] != $grupAnterior) {
            $grupAnterior = $item['grup'];

            $grup['grups'][$grupAnterior] = array();
          }

          array_push($grup['grups'][$grupAnterior], $item['item']);
        }

        if (key_exists('grups', $grup) && count($grup['grups']) > 0) {
          array_push($res, $grup);
        }

        $event->count = count($temp);
        $event->items = $res;
      } elseif (key_exists('tipus', $event->options) && $event->options['tipus'] == 'Publicacions') {

        // aplanamos array
        $temp = array();

        foreach ($event->target as $any) {
          foreach ($any['items'] as $item) {
            array_push($temp, array(
                'any' => $any['any'],
                'item' => $item
            ));
          }
        }

        $items = array_slice($temp, $event->getOffset(), $event->getLimit());

        // recuperamos estructura original

        $rec = array();
        $it = array();
        $anyAnterior = '';

        foreach ($items as $item) {

          if ($item['any'] != $anyAnterior) {

            if (key_exists('items', $it) && count($it['items']) > 0) {
              array_push($rec, $it);
            }

            $anyAnterior = $item['any'];

            $it = array(
                'any' => $anyAnterior,
                'items' => array()
            );
          }

          array_push($it['items'], $item['item']);
        }

        if (key_exists('items', $it) && count($it['items']) > 0) {
          array_push($rec, $it);
        }

        $event->count = count($temp);
        $event->items = $rec;
      } else {
        $event->count = count($event->target);
        $event->items = array_slice($event->target, $event->getOffset(), $event->getLimit());
      }


      $event->stopPropagation();
    }
  }

  public static function getSubscribedEvents() {
    return array(
        'knp_pager.items' => array('items', 1)
    );
  }

}
