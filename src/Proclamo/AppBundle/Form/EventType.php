<?php

namespace Proclamo\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EventType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $now = new \DateTime('NOW');

        $builder
                ->add('titol', 'text', array('attr' => array('size' => '70')))
                ->add('data', 'date', array(
                    'label' => "Data de l'event",
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'required' => true,
                    'attr' => array(
                        'placeholder' => 'ex. ' . $now->format('d/m/Y'),
                        'pattern' => '\d{2}/\d{2}\/\d{4}'
                    )
                ))
                ->add('hora', 'time', array(
                    'widget' => 'single_text',
                    'required' => false
                ))
                ->add('texte', 'textarea', array('attr' => array(
                        'cols' => '40',
                        'rows' => '5',
                        'class' => 'tinymce',
                        'data-theme' => 'modern'
            )))
                ->add('foto', new ImageType(), array('label' => 'Foto', 'required' => false))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Proclamo\AppBundle\Entity\Event'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'proclamo_appbundle_event';
    }

}
