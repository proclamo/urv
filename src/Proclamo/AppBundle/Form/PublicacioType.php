<?php

namespace Proclamo\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PublicacioType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('grup', 'text', array('label' => 'Agrupar per', 'required' => true, 'attr' => array('class' => 'translatable')))
            ->add('any')
            ->add('titol', 'text', array('attr' => array('class' => 'translatable', 'size' => 50)))
            ->add('referencia', 'textarea', array('attr' => array(
                        'cols' => '25',
                        'rows' => '5',
                        'class' => 'tinymce',
                        'data-theme' => 'modern'
                    )))    
            ->add('ciutat', 'text', array('required' => false))    
            ->add('pais', 'text', array('required' => false))    
            ->add('nomMedi', 'text', array('label' => 'Nom del medi', 'required' => false))    
            ->add('volum', 'text', array('label' => 'Volum', 'required' => false))
            ->add('pagines', 'text', array('label' => 'Pàgines', 'required' => false))
            ->add('autors', 'collection',
                array(
                    "required" => false,
                    "type" => new AutorsType(),
                    "allow_add" => true,
                    "allow_delete" => true,
                    "by_reference" => false,
                    "label" => false
                )
            )
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Proclamo\AppBundle\Entity\Publicacio'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'proclamo_appbundle_publicacio';
    }
}
