<?php

namespace Proclamo\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of PersonalRefType
 *
 * @author cristianmartin
 */
class AutorsType extends AbstractType {
    
   public function buildForm(FormBuilderInterface $builder, array $options)
    {        
        $builder->add('id', 'hidden', array("label" => false));
    }

    public function getName()
    {
        return 'proclamo_appbundle_autorstype';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Proclamo\AppBundle\Entity\PublicacioAutors',
        ));
    } 
}
