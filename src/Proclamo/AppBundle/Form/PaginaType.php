<?php

namespace Proclamo\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PaginaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('grup', 'choice', array('label' => 'Menú', 'choices' => array('departament' => 'Departament', 'grau' => 'Estudis de grau', 'postgrau' => 'Estudis de postgrau', 'recerca' => 'Recerca'), 'required' => true))    
            ->add('titol', 'text', array('attr' => array('class' => 'translatable'), 'label' => 'Títol'))
            ->add('texte', 'textarea', array('attr' => array(
                'cols' => '50' , 
                'rows' => '3', 
                'class' => 'translatable tinymce',
                'data-theme' => 'modern'
                ),
                'label' => 'Texte (traduïble)'))
            ->add('foto', new ImageType(), array('label' => 'Foto', 'required' => false))      
            ->add('ordre', 'integer', array('label' => 'Posició'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Proclamo\AppBundle\Entity\Pagina'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'proclamo_appbundle_pagina';
    }
}
