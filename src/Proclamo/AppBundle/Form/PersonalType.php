<?php

namespace Proclamo\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PersonalType extends AbstractType
{
    private $helper;
    
    public function setPersonalHelper($helper)
    {
        $this->helper = $helper;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $now = new \DateTime('NOW');
        
        $builder
            ->add('nom', 'text', array('attr' => array('size' => '70')))
            ->add('cognoms', 'text', array('attr' => array('size' => '70')))
            ->add('tipus', 'choice', array('choices' => $this->helper->getCategoriaProfesional() , 'label' => 'Categoria professional'))
            ->add('situacio', 'choice', array('choices' => $this->helper->getSituacioAdministrativa(), 'label' => 'Situació administrativa'))
            ->add('jornada', 'choice', array('choices' => $this->helper->getDedicacio(), 'label' => 'Dedicació'))
            ->add('dataAlta', 'date', array(
                    'label' => "Data d'alta",
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'required' => false,
                    'attr' => array(
                        'placeholder' => 'ex. ' . $now->format('d/m/Y'),
                        'pattern' => '\d{2}/\d{2}\/\d{4}'
                    )
                ))
            ->add('dataBaixa', 'date', array(
                    'label' => "Data de baixa",
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'required' => false,
                    'attr' => array(
                        'placeholder' => 'ex. ' . $now->format('d/m/Y'),
                        'pattern' => '\d{2}/\d{2}\/\d{4}'
                    )
                ))    
            ->add('email', 'email', array('attr' => array('size' => '70'), 'required' => false))
            ->add('telefon','text', array('label' => 'Telèfon', 'attr' => array('size' => '70'), 'required' => false))
            ->add('despatx', 'text', array('required' => false, 'attr' => array('size' => '70')))
            ->add('perfil', 'textarea', array('attr' => array('cols' => '69' , 'rows' => '3', 'class' => 'translatable'), 'required' => false))
            ->add('especialitat', 'choice', array('choices' => $this->helper->getEspecialitat(), 'empty_value' => '', 'label' => 'Camp àrea de coneixement', 'required' => false))
            ->add('grup', 'text', array('label' => 'Grup de recerca consolidat', 'attr' => array('size' => '70', 'class' => 'translatable'), 'required' => false))
            ->add('grupurv', 'text', array('label' => 'Grup de recerca URV', 'attr' => array('size' => '70', 'class' => 'translatable'), 'required' => false))
            ->add('curriculum', new PDFType(), array('label' => 'Currículum', 'required' => false))
            ->add('cvbreu', new PDFType(), array('label' => 'Currículum abreujat', 'required' => false))
            ->add('foto', new ImageType(), array('label' => 'Foto', 'required' => false))    
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Proclamo\AppBundle\Entity\Personal',
            'cascade_validation' => true
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'proclamo_appbundle_personal';
    }
}
