<?php

namespace Proclamo\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NoticiaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('titol', 'text', array('attr' => array('size' => '70')))
                ->add('texte', 'textarea', array('attr' => array(
                        'cols' => '40',
                        'rows' => '5',
                        'class' => 'tinymce',
                        'data-theme' => 'modern'
                    )))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Proclamo\AppBundle\Entity\Noticia'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'proclamo_appbundle_noticia';
    }

}
