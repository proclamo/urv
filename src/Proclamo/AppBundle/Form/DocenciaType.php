<?php

namespace Proclamo\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DocenciaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder            
            ->add('nom', null, array('label' =>  'Nom a mostrar', 'required' => true, 'attr' => array('class' => 'translatable')))
            ->add('ruta', null, array('label' =>  'Ruta', 'required' => true))
            ->add('posicio', null, array('label' => 'Ordre', 'required' => true));
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Proclamo\AppBundle\Entity\Docencia'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'proclamo_appbundle_docencia';
    }
}
