<?php

namespace Proclamo\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of ModelsType
 *
 * @author cristianmartin
 */
class ModelsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('grup', 'text', array('required' => false))
            ->add('nom', 'text', array('required' => true))            
            ->add('arxiu', new PDFType(), array('label' => 'Arxiu', 'required' => false))
            ->add('posicio', 'integer', array('label' => 'Posició'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Proclamo\AppBundle\Entity\Model'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'proclamo_appbundle_model';
    }
}
