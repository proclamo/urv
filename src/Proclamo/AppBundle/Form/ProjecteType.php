<?php

namespace Proclamo\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProjecteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('grup', 'text', array('label' => 'Agrupar per', 'required' => false, 'attr' => array('class' => 'translatable')))
            ->add('titol')
            //->add('investigador', 'text', array('label' => 'Investigador principal'))            
            ->add('inici', 'integer', array('label' => "Any d'inici"))
            ->add('final', 'integer', array('label' => "Any de finalització", 'required' => false))
            ->add('codi')
            ->add('descripcio', 'textarea', array('attr' => array('cols' => '50' , 'rows' => '3')))
            ->add('link')    
            ->add('autors', 'collection',
                array(
                    "required" => false,
                    "type" => new PersonalRefType(),
                    "allow_add" => true,
                    "allow_delete" => true,
                    "by_reference" => false,
                    "label" => false
                )
            )
            ->add('colaboradors', 'collection',
                array(
                    "required" => false,
                    "type" => new ColaboradorRefType(),
                    "allow_add" => true,
                    "allow_delete" => true,
                    "by_reference" => false,
                    "label" => false
                )
            )    
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Proclamo\AppBundle\Entity\Projecte'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'proclamo_appbundle_projecte';
    }
}
